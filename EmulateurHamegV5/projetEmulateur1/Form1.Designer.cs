﻿namespace projetEmulateur1
{
    partial class imgOutpOn
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(imgOutpOn));
            this.cboPort = new System.Windows.Forms.ComboBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtVoltCh1 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtAmpCh1 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtAmpCh2 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtVoltCh2 = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtAmpCh4 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtVoltCh4 = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtAmpCh3 = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtVoltCh3 = new System.Windows.Forms.TextBox();
            this.Ch1Gris = new System.Windows.Forms.PictureBox();
            this.Ch4Bleu = new System.Windows.Forms.PictureBox();
            this.Ch3Bleu = new System.Windows.Forms.PictureBox();
            this.Ch1Bleu = new System.Windows.Forms.PictureBox();
            this.Ch1Vert = new System.Windows.Forms.PictureBox();
            this.Ch4Gris = new System.Windows.Forms.PictureBox();
            this.Ch2Bleu = new System.Windows.Forms.PictureBox();
            this.Ch4Vert = new System.Windows.Forms.PictureBox();
            this.Ch3Vert = new System.Windows.Forms.PictureBox();
            this.Ch2Vert = new System.Windows.Forms.PictureBox();
            this.Ch3Gris = new System.Windows.Forms.PictureBox();
            this.Ch2Gris = new System.Windows.Forms.PictureBox();
            this.Rmt = new System.Windows.Forms.PictureBox();
            this.OutpGris = new System.Windows.Forms.PictureBox();
            this.OutpBlanc = new System.Windows.Forms.PictureBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Ch1Gris)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch4Bleu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch3Bleu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch1Bleu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch1Vert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch4Gris)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch2Bleu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch4Vert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch3Vert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch2Vert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch3Gris)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch2Gris)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rmt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutpGris)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutpBlanc)).BeginInit();
            this.SuspendLayout();
            // 
            // cboPort
            // 
            this.cboPort.FormattingEnabled = true;
            this.cboPort.Location = new System.Drawing.Point(107, 15);
            this.cboPort.Margin = new System.Windows.Forms.Padding(2);
            this.cboPort.MaximumSize = new System.Drawing.Size(115, 0);
            this.cboPort.MinimumSize = new System.Drawing.Size(115, 0);
            this.cboPort.Name = "cboPort";
            this.cboPort.Size = new System.Drawing.Size(115, 21);
            this.cboPort.TabIndex = 0;
            this.cboPort.SelectedIndexChanged += new System.EventHandler(this.cboPort_SelectedIndexChanged);
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(226, 15);
            this.btnOpen.Margin = new System.Windows.Forms.Padding(2);
            this.btnOpen.MaximumSize = new System.Drawing.Size(79, 21);
            this.btnOpen.MinimumSize = new System.Drawing.Size(79, 21);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(79, 21);
            this.btnOpen.TabIndex = 1;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(309, 15);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.MaximumSize = new System.Drawing.Size(79, 21);
            this.btnClose.MinimumSize = new System.Drawing.Size(79, 21);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(79, 21);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(76, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Port:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtVoltCh1);
            this.groupBox1.Location = new System.Drawing.Point(98, 103);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.MaximumSize = new System.Drawing.Size(137, 39);
            this.groupBox1.MinimumSize = new System.Drawing.Size(137, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(137, 39);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Volt";
            // 
            // txtVoltCh1
            // 
            this.txtVoltCh1.Location = new System.Drawing.Point(27, 12);
            this.txtVoltCh1.Margin = new System.Windows.Forms.Padding(2);
            this.txtVoltCh1.Multiline = true;
            this.txtVoltCh1.Name = "txtVoltCh1";
            this.txtVoltCh1.ReadOnly = true;
            this.txtVoltCh1.Size = new System.Drawing.Size(108, 24);
            this.txtVoltCh1.TabIndex = 16;
            this.txtVoltCh1.TextChanged += new System.EventHandler(this.txtVoltCh1_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtAmpCh1);
            this.groupBox2.Location = new System.Drawing.Point(235, 103);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.MaximumSize = new System.Drawing.Size(133, 39);
            this.groupBox2.MinimumSize = new System.Drawing.Size(133, 39);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(133, 39);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ampere";
            // 
            // txtAmpCh1
            // 
            this.txtAmpCh1.Location = new System.Drawing.Point(16, 12);
            this.txtAmpCh1.Margin = new System.Windows.Forms.Padding(2);
            this.txtAmpCh1.Multiline = true;
            this.txtAmpCh1.Name = "txtAmpCh1";
            this.txtAmpCh1.ReadOnly = true;
            this.txtAmpCh1.Size = new System.Drawing.Size(105, 24);
            this.txtAmpCh1.TabIndex = 17;
            this.txtAmpCh1.TextChanged += new System.EventHandler(this.txtAmpCh1_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtAmpCh2);
            this.groupBox3.Location = new System.Drawing.Point(235, 138);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.MaximumSize = new System.Drawing.Size(133, 39);
            this.groupBox3.MinimumSize = new System.Drawing.Size(133, 39);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(133, 39);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ampere";
            // 
            // txtAmpCh2
            // 
            this.txtAmpCh2.Location = new System.Drawing.Point(16, 12);
            this.txtAmpCh2.Margin = new System.Windows.Forms.Padding(2);
            this.txtAmpCh2.Multiline = true;
            this.txtAmpCh2.Name = "txtAmpCh2";
            this.txtAmpCh2.ReadOnly = true;
            this.txtAmpCh2.Size = new System.Drawing.Size(105, 24);
            this.txtAmpCh2.TabIndex = 17;
            this.txtAmpCh2.TextChanged += new System.EventHandler(this.txtAmpCh2_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtVoltCh2);
            this.groupBox4.Location = new System.Drawing.Point(98, 138);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.MaximumSize = new System.Drawing.Size(137, 39);
            this.groupBox4.MinimumSize = new System.Drawing.Size(137, 39);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(137, 39);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Volt";
            // 
            // txtVoltCh2
            // 
            this.txtVoltCh2.Location = new System.Drawing.Point(27, 12);
            this.txtVoltCh2.Margin = new System.Windows.Forms.Padding(2);
            this.txtVoltCh2.Multiline = true;
            this.txtVoltCh2.Name = "txtVoltCh2";
            this.txtVoltCh2.ReadOnly = true;
            this.txtVoltCh2.Size = new System.Drawing.Size(108, 24);
            this.txtVoltCh2.TabIndex = 18;
            this.txtVoltCh2.TextChanged += new System.EventHandler(this.txtVoltCh2_TextChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtAmpCh4);
            this.groupBox5.Location = new System.Drawing.Point(235, 213);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.MaximumSize = new System.Drawing.Size(133, 46);
            this.groupBox5.MinimumSize = new System.Drawing.Size(133, 46);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(133, 46);
            this.groupBox5.TabIndex = 31;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ampere";
            // 
            // txtAmpCh4
            // 
            this.txtAmpCh4.Location = new System.Drawing.Point(16, 12);
            this.txtAmpCh4.Margin = new System.Windows.Forms.Padding(2);
            this.txtAmpCh4.Multiline = true;
            this.txtAmpCh4.Name = "txtAmpCh4";
            this.txtAmpCh4.ReadOnly = true;
            this.txtAmpCh4.Size = new System.Drawing.Size(105, 24);
            this.txtAmpCh4.TabIndex = 17;
            this.txtAmpCh4.TextChanged += new System.EventHandler(this.txtAmpCh4_TextChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtVoltCh4);
            this.groupBox6.Location = new System.Drawing.Point(98, 213);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.MaximumSize = new System.Drawing.Size(137, 46);
            this.groupBox6.MinimumSize = new System.Drawing.Size(137, 46);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(137, 46);
            this.groupBox6.TabIndex = 30;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Volt";
            // 
            // txtVoltCh4
            // 
            this.txtVoltCh4.Location = new System.Drawing.Point(27, 12);
            this.txtVoltCh4.Margin = new System.Windows.Forms.Padding(2);
            this.txtVoltCh4.Multiline = true;
            this.txtVoltCh4.Name = "txtVoltCh4";
            this.txtVoltCh4.ReadOnly = true;
            this.txtVoltCh4.Size = new System.Drawing.Size(108, 24);
            this.txtVoltCh4.TabIndex = 18;
            this.txtVoltCh4.TextChanged += new System.EventHandler(this.txtVoltCh4_TextChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtAmpCh3);
            this.groupBox7.Location = new System.Drawing.Point(235, 177);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox7.MaximumSize = new System.Drawing.Size(133, 39);
            this.groupBox7.MinimumSize = new System.Drawing.Size(133, 39);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox7.Size = new System.Drawing.Size(133, 39);
            this.groupBox7.TabIndex = 29;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Ampere";
            // 
            // txtAmpCh3
            // 
            this.txtAmpCh3.Location = new System.Drawing.Point(16, 12);
            this.txtAmpCh3.Margin = new System.Windows.Forms.Padding(2);
            this.txtAmpCh3.Multiline = true;
            this.txtAmpCh3.Name = "txtAmpCh3";
            this.txtAmpCh3.ReadOnly = true;
            this.txtAmpCh3.Size = new System.Drawing.Size(105, 24);
            this.txtAmpCh3.TabIndex = 17;
            this.txtAmpCh3.TextChanged += new System.EventHandler(this.txtAmpCh3_TextChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.txtVoltCh3);
            this.groupBox8.Location = new System.Drawing.Point(98, 177);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox8.MaximumSize = new System.Drawing.Size(137, 39);
            this.groupBox8.MinimumSize = new System.Drawing.Size(137, 39);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox8.Size = new System.Drawing.Size(137, 39);
            this.groupBox8.TabIndex = 28;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Volt";
            // 
            // txtVoltCh3
            // 
            this.txtVoltCh3.Location = new System.Drawing.Point(27, 12);
            this.txtVoltCh3.Margin = new System.Windows.Forms.Padding(2);
            this.txtVoltCh3.Multiline = true;
            this.txtVoltCh3.Name = "txtVoltCh3";
            this.txtVoltCh3.ReadOnly = true;
            this.txtVoltCh3.Size = new System.Drawing.Size(108, 24);
            this.txtVoltCh3.TabIndex = 16;
            this.txtVoltCh3.TextChanged += new System.EventHandler(this.txtVoltCh3_TextChanged);
            // 
            // Ch1Gris
            // 
            this.Ch1Gris.Image = ((System.Drawing.Image)(resources.GetObject("Ch1Gris.Image")));
            this.Ch1Gris.Location = new System.Drawing.Point(554, 86);
            this.Ch1Gris.Margin = new System.Windows.Forms.Padding(2);
            this.Ch1Gris.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch1Gris.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch1Gris.Name = "Ch1Gris";
            this.Ch1Gris.Size = new System.Drawing.Size(60, 34);
            this.Ch1Gris.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch1Gris.TabIndex = 32;
            this.Ch1Gris.TabStop = false;
            this.Ch1Gris.Click += new System.EventHandler(this.Ch1Gris_Click);
            // 
            // Ch4Bleu
            // 
            this.Ch4Bleu.Image = ((System.Drawing.Image)(resources.GetObject("Ch4Bleu.Image")));
            this.Ch4Bleu.Location = new System.Drawing.Point(746, 86);
            this.Ch4Bleu.Margin = new System.Windows.Forms.Padding(2);
            this.Ch4Bleu.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch4Bleu.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch4Bleu.Name = "Ch4Bleu";
            this.Ch4Bleu.Size = new System.Drawing.Size(60, 34);
            this.Ch4Bleu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch4Bleu.TabIndex = 33;
            this.Ch4Bleu.TabStop = false;
            this.Ch4Bleu.Click += new System.EventHandler(this.Ch4Bleu_Click);
            // 
            // Ch3Bleu
            // 
            this.Ch3Bleu.Image = ((System.Drawing.Image)(resources.GetObject("Ch3Bleu.Image")));
            this.Ch3Bleu.Location = new System.Drawing.Point(682, 86);
            this.Ch3Bleu.Margin = new System.Windows.Forms.Padding(2);
            this.Ch3Bleu.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch3Bleu.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch3Bleu.Name = "Ch3Bleu";
            this.Ch3Bleu.Size = new System.Drawing.Size(60, 34);
            this.Ch3Bleu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch3Bleu.TabIndex = 34;
            this.Ch3Bleu.TabStop = false;
            this.Ch3Bleu.Click += new System.EventHandler(this.Ch3Bleu_Click);
            // 
            // Ch1Bleu
            // 
            this.Ch1Bleu.Image = ((System.Drawing.Image)(resources.GetObject("Ch1Bleu.Image")));
            this.Ch1Bleu.Location = new System.Drawing.Point(554, 86);
            this.Ch1Bleu.Margin = new System.Windows.Forms.Padding(2);
            this.Ch1Bleu.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch1Bleu.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch1Bleu.Name = "Ch1Bleu";
            this.Ch1Bleu.Size = new System.Drawing.Size(60, 34);
            this.Ch1Bleu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch1Bleu.TabIndex = 35;
            this.Ch1Bleu.TabStop = false;
            this.Ch1Bleu.Click += new System.EventHandler(this.Ch1Bleu_Click);
            // 
            // Ch1Vert
            // 
            this.Ch1Vert.Image = ((System.Drawing.Image)(resources.GetObject("Ch1Vert.Image")));
            this.Ch1Vert.Location = new System.Drawing.Point(554, 86);
            this.Ch1Vert.Margin = new System.Windows.Forms.Padding(2);
            this.Ch1Vert.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch1Vert.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch1Vert.Name = "Ch1Vert";
            this.Ch1Vert.Size = new System.Drawing.Size(60, 34);
            this.Ch1Vert.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch1Vert.TabIndex = 36;
            this.Ch1Vert.TabStop = false;
            this.Ch1Vert.Click += new System.EventHandler(this.Ch1Vert_Click);
            // 
            // Ch4Gris
            // 
            this.Ch4Gris.Image = ((System.Drawing.Image)(resources.GetObject("Ch4Gris.Image")));
            this.Ch4Gris.Location = new System.Drawing.Point(746, 86);
            this.Ch4Gris.Margin = new System.Windows.Forms.Padding(2);
            this.Ch4Gris.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch4Gris.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch4Gris.Name = "Ch4Gris";
            this.Ch4Gris.Size = new System.Drawing.Size(60, 34);
            this.Ch4Gris.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch4Gris.TabIndex = 37;
            this.Ch4Gris.TabStop = false;
            this.Ch4Gris.Click += new System.EventHandler(this.Ch4Gris_Click);
            // 
            // Ch2Bleu
            // 
            this.Ch2Bleu.Image = ((System.Drawing.Image)(resources.GetObject("Ch2Bleu.Image")));
            this.Ch2Bleu.Location = new System.Drawing.Point(618, 86);
            this.Ch2Bleu.Margin = new System.Windows.Forms.Padding(2);
            this.Ch2Bleu.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch2Bleu.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch2Bleu.Name = "Ch2Bleu";
            this.Ch2Bleu.Size = new System.Drawing.Size(60, 34);
            this.Ch2Bleu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch2Bleu.TabIndex = 38;
            this.Ch2Bleu.TabStop = false;
            this.Ch2Bleu.Click += new System.EventHandler(this.Ch2Bleu_Click);
            // 
            // Ch4Vert
            // 
            this.Ch4Vert.Image = ((System.Drawing.Image)(resources.GetObject("Ch4Vert.Image")));
            this.Ch4Vert.Location = new System.Drawing.Point(746, 86);
            this.Ch4Vert.Margin = new System.Windows.Forms.Padding(2);
            this.Ch4Vert.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch4Vert.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch4Vert.Name = "Ch4Vert";
            this.Ch4Vert.Size = new System.Drawing.Size(60, 34);
            this.Ch4Vert.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch4Vert.TabIndex = 39;
            this.Ch4Vert.TabStop = false;
            this.Ch4Vert.Click += new System.EventHandler(this.Ch4Vert_Click);
            // 
            // Ch3Vert
            // 
            this.Ch3Vert.Image = ((System.Drawing.Image)(resources.GetObject("Ch3Vert.Image")));
            this.Ch3Vert.Location = new System.Drawing.Point(682, 86);
            this.Ch3Vert.Margin = new System.Windows.Forms.Padding(2);
            this.Ch3Vert.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch3Vert.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch3Vert.Name = "Ch3Vert";
            this.Ch3Vert.Size = new System.Drawing.Size(60, 34);
            this.Ch3Vert.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch3Vert.TabIndex = 40;
            this.Ch3Vert.TabStop = false;
            this.Ch3Vert.Click += new System.EventHandler(this.Ch3Vert_Click);
            // 
            // Ch2Vert
            // 
            this.Ch2Vert.Image = ((System.Drawing.Image)(resources.GetObject("Ch2Vert.Image")));
            this.Ch2Vert.Location = new System.Drawing.Point(618, 86);
            this.Ch2Vert.Margin = new System.Windows.Forms.Padding(2);
            this.Ch2Vert.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch2Vert.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch2Vert.Name = "Ch2Vert";
            this.Ch2Vert.Size = new System.Drawing.Size(60, 34);
            this.Ch2Vert.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch2Vert.TabIndex = 41;
            this.Ch2Vert.TabStop = false;
            this.Ch2Vert.Click += new System.EventHandler(this.Ch2Vert_Click);
            // 
            // Ch3Gris
            // 
            this.Ch3Gris.Image = ((System.Drawing.Image)(resources.GetObject("Ch3Gris.Image")));
            this.Ch3Gris.Location = new System.Drawing.Point(682, 86);
            this.Ch3Gris.Margin = new System.Windows.Forms.Padding(2);
            this.Ch3Gris.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch3Gris.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch3Gris.Name = "Ch3Gris";
            this.Ch3Gris.Size = new System.Drawing.Size(60, 34);
            this.Ch3Gris.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch3Gris.TabIndex = 42;
            this.Ch3Gris.TabStop = false;
            this.Ch3Gris.Click += new System.EventHandler(this.Ch3Gris_Click);
            // 
            // Ch2Gris
            // 
            this.Ch2Gris.Image = ((System.Drawing.Image)(resources.GetObject("Ch2Gris.Image")));
            this.Ch2Gris.Location = new System.Drawing.Point(618, 86);
            this.Ch2Gris.Margin = new System.Windows.Forms.Padding(2);
            this.Ch2Gris.MaximumSize = new System.Drawing.Size(60, 34);
            this.Ch2Gris.MinimumSize = new System.Drawing.Size(60, 34);
            this.Ch2Gris.Name = "Ch2Gris";
            this.Ch2Gris.Size = new System.Drawing.Size(60, 34);
            this.Ch2Gris.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Ch2Gris.TabIndex = 43;
            this.Ch2Gris.TabStop = false;
            this.Ch2Gris.Click += new System.EventHandler(this.Ch2Gris_Click);
            // 
            // Rmt
            // 
            this.Rmt.Image = ((System.Drawing.Image)(resources.GetObject("Rmt.Image")));
            this.Rmt.Location = new System.Drawing.Point(809, 224);
            this.Rmt.Margin = new System.Windows.Forms.Padding(2);
            this.Rmt.MaximumSize = new System.Drawing.Size(45, 33);
            this.Rmt.MinimumSize = new System.Drawing.Size(45, 33);
            this.Rmt.Name = "Rmt";
            this.Rmt.Size = new System.Drawing.Size(45, 33);
            this.Rmt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Rmt.TabIndex = 44;
            this.Rmt.TabStop = false;
            this.Rmt.Click += new System.EventHandler(this.Rmt_Click);
            // 
            // OutpGris
            // 
            this.OutpGris.Image = ((System.Drawing.Image)(resources.GetObject("OutpGris.Image")));
            this.OutpGris.Location = new System.Drawing.Point(809, 86);
            this.OutpGris.Margin = new System.Windows.Forms.Padding(2);
            this.OutpGris.MaximumSize = new System.Drawing.Size(60, 34);
            this.OutpGris.MinimumSize = new System.Drawing.Size(60, 34);
            this.OutpGris.Name = "OutpGris";
            this.OutpGris.Size = new System.Drawing.Size(60, 34);
            this.OutpGris.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.OutpGris.TabIndex = 45;
            this.OutpGris.TabStop = false;
            this.OutpGris.Click += new System.EventHandler(this.OutpGris_Click);
            // 
            // OutpBlanc
            // 
            this.OutpBlanc.Image = ((System.Drawing.Image)(resources.GetObject("OutpBlanc.Image")));
            this.OutpBlanc.Location = new System.Drawing.Point(809, 86);
            this.OutpBlanc.Margin = new System.Windows.Forms.Padding(2);
            this.OutpBlanc.MaximumSize = new System.Drawing.Size(60, 34);
            this.OutpBlanc.MinimumSize = new System.Drawing.Size(60, 34);
            this.OutpBlanc.Name = "OutpBlanc";
            this.OutpBlanc.Size = new System.Drawing.Size(60, 34);
            this.OutpBlanc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.OutpBlanc.TabIndex = 46;
            this.OutpBlanc.TabStop = false;
            this.OutpBlanc.Click += new System.EventHandler(this.OutpBlanc_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(886, 2);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(285, 524);
            this.listBox1.TabIndex = 47;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Location = new System.Drawing.Point(-3, -1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(895, 525);
            this.panel2.TabIndex = 49;
            // 
            // imgOutpOn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1170, 525);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.OutpBlanc);
            this.Controls.Add(this.OutpGris);
            this.Controls.Add(this.Rmt);
            this.Controls.Add(this.Ch2Gris);
            this.Controls.Add(this.Ch3Gris);
            this.Controls.Add(this.Ch2Vert);
            this.Controls.Add(this.Ch3Vert);
            this.Controls.Add(this.Ch4Vert);
            this.Controls.Add(this.Ch2Bleu);
            this.Controls.Add(this.Ch4Gris);
            this.Controls.Add(this.Ch1Vert);
            this.Controls.Add(this.Ch1Bleu);
            this.Controls.Add(this.Ch3Bleu);
            this.Controls.Add(this.Ch4Bleu);
            this.Controls.Add(this.Ch1Gris);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.cboPort);
            this.Controls.Add(this.panel2);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(907, 564);
            this.Name = "imgOutpOn";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Ch1Gris)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch4Bleu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch3Bleu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch1Bleu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch1Vert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch4Gris)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch2Bleu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch4Vert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch3Vert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch2Vert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch3Gris)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ch2Gris)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rmt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutpGris)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutpBlanc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboPort;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label2;
        private System.IO.Ports.SerialPort serialPort1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtVoltCh1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtAmpCh1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtAmpCh2;
        private System.Windows.Forms.TextBox txtVoltCh2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtAmpCh4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtVoltCh4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtAmpCh3;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox txtVoltCh3;
        private System.Windows.Forms.PictureBox Ch1Gris;
        private System.Windows.Forms.PictureBox Ch4Bleu;
        private System.Windows.Forms.PictureBox Ch3Bleu;
        private System.Windows.Forms.PictureBox Ch1Bleu;
        private System.Windows.Forms.PictureBox Ch1Vert;
        private System.Windows.Forms.PictureBox Ch4Gris;
        private System.Windows.Forms.PictureBox Ch2Bleu;
        private System.Windows.Forms.PictureBox Ch4Vert;
        private System.Windows.Forms.PictureBox Ch3Vert;
        private System.Windows.Forms.PictureBox Ch2Vert;
        private System.Windows.Forms.PictureBox Ch3Gris;
        private System.Windows.Forms.PictureBox Ch2Gris;
        private System.Windows.Forms.PictureBox Rmt;
        private System.Windows.Forms.PictureBox OutpGris;
        private System.Windows.Forms.PictureBox OutpBlanc;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Panel panel2;
    }
}

