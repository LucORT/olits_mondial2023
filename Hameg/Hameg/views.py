from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
import os
from json import dump, load
import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse

# Définition d'une fonction home
def home(request):
    # Prend le fichier dans template
    template = loader.get_template('home.html')
    # Appelé body.html
    return HttpResponse(template.render())
    # Renvoi la page web en réponse
