import sys
import os
import time
import serial
import platform
import serial.tools.list_ports as port_list #imporatation de la liste des ports
if(platform.system() == "Windows"):
    from serial.serialwin32 import Serial
    from serial.tools.list_ports_windows import NULL
    import serial.tools.list_ports
if(platform.system() == "Linux"):
    from serial import Serial

from Channel import channel

# EL import serial.tools.list_ports as port_list
# import socket
# import serial
# from typing import Container
# from serial import serialwin32
# import serial.tools.list_ports as port_list
# from serial.win32 import ONE5STOPBITS
# import time
# import "Channel.py"
# sys.path.append('/Channel.py')
#  TOUT NULL REMPLACE PAR NONE POUR LA COMPATIBILITE LINUX

encoding = 'utf-8'
decoding = 'utf-8'

# Classe HMP4040
class HMP4040 :
    cPort = ""
    oPort = serial.Serial()
    Channels = [1, 2, 3, 4]
    listPortHameg = {}
    DEBUG = False

    # Constructeur de la classe
    # poPort -> Variable à rentrer par l'utilisateur (numéro du port)
    def __init__(self, numport=None):
# am 20230320 début
        self.system = platform.system()  #variable qui permet de vérifier le system
        self.iNumChannel = 1
        if (numport != None):
            self.cPort = numport
            self.pOpen()
        #popen return bool avec un trycatch
        for i in range(0, 4):
            self.Channels[i] = channel(self, i + 1)



# am 20230320 fin
    # Destructeur de la classe
    # Ferme le serial
#    def __del__(self):
#if(self.oPort.is_open == True):
   #   self.oPort.close()
    def setCPort(self, numport):
        #port = "COM" + str(numport)
        self.cPort = numport
        status = self.pOpen()
        return status

    def pOpen(self):
        try:
            if self.system == "Linux":
                print(self.cPort)
                self.oPort = Serial(port=self.cPort, baudrate=9600, timeout=1,writeTimeout=2)
            else:
                self.oPort = Serial(port=self.cPort, baudrate=9600, timeout=1,writeTimeout=2)
            return True
        except:
            return False

    def piloter(self, channel, intensity, tension):
        try:
            if self.defOpen():
                #self.oPort.write(bytes(str + '\n', 'utf-8'))
                self.ecrire("INST OUT" + str(channel))
                self.ecrire("APPLY " + str(tension) + ',' + str(intensity))
                # time.sleep(1)
                return "Modification de la tension max et de l''intensité max OK"
            # else:
                # print(f"Modification de la tension max et de l''intensité max impossible sur channel {channel} : le port n'est pas ouvert")
                
        except Exception as e:
            # print(f"Modification de la tension max et de l''intensité max impossible sur channel {channel} :  {e}")
            return self.cPort
        finally:
            self.defClose()

            # Modifie les valeurs de la tension max
            # et de l'intensité max du channel visé
    # Méthode Test Connexion
    def lireLigne(self):
        if (False):
            # cas du readline réel
            line = self.oPort.readlines()
        else:
            # on utilise notre propre readline
            line = ""
            car=""
            while(car != b"\n"):
                car = self.oPort.read()
                if car != b"\n":
                    line += car.decode("utf-8")
                    self.printD(car)
        
        return line
    
    def ecrire (self, str):
        self.printD("j'envoie : "+str)
        self.oPort.write(bytes(str+'\n', 'utf-8'))
    
    def defOpen(self, max_attempts=5, delay=1):
        """
        Essaie d'ouvrir le port série avec un nombre maximum de tentatives.
        
        Arguments:
        - max_attempts (int): Nombre maximum de tentatives avant d'abandonner.
        - delay (int): Délai en secondes entre chaque tentative.
        
        Retourne:
        - True si le port est ouvert avec succès.
        - False sinon.
        """
        if not self.oPort.is_open:
            self.printD("Je me connecte")
            self.oPort.timeout = 5000
            attempts = 0
            
            # while attempts < max_attempts:
            try:
                # print(f"Tentative {attempts + 1}/{max_attempts}: Ouverture du port série...")
                if self.oPort.is_open:
                    pass  # Succès : le port est ouvert
                else:
                    self.oPort.open()
                print(f"Port série ouvert avec succès, tentative {attempts + 1}/{max_attempts}")
                return True  # Succès : le port est ouvert
                    
            except Exception as e:
                print(f"Tentative {attempts + 1}/{max_attempts} échouée : {e}")
                attempts += 1
                time.sleep(delay)  # Pause avant de réessayer
            
            # print("Échec après plusieurs tentatives d'ouverture du port série.")
            # return False  # Échec après toutes les tentatives
        
        else:
            print("Le port série est déjà ouvert.")
            return True
        
    def defClose(self):
        if self.oPort.is_open:
            self.printD("Je me déconnecte")
            try:
                self.oPort.close()
                # print("Port fermé avec succès.")
                return True
            except Exception as e:
                # print(f"Erreur lors de la fermeture du port : {e}")
                return False
        else:
            # print("Le port n'était pas ouvert.")
            return True  # Le port était déjà fermé, pas d'erreur

    # Méthode Test Connexion
    def TestConnexion(self):
        #if (self.oPort.is_open):
         #   self.defClose()
        if(self.pOpen()):
            #self.printD("J'essaie de me connecter")
            OK = "ko"
            self.defClose()
            #self.printD("port is open ? :",self.oPort.is_open)
            if(self.defOpen()):
               if(self.oPort.is_open):
                  #self.oPort.writelines("*IDN?")
                  self.ecrire('*IDN?')
                  #self.printD('etape 1')
                  res = self.lireLigne()
                  #self.printD('etape 1b')
                  # res = self.oPort.readlines()
                  #self.printD('etape 2')
                  #self.printD(res)
                  #self.printD('etape 3')
                  if(res[0:13] == "HAMEG,HMP4040"):
                    #self.printD('Connexion réussie.')
                    OK = "ok"
                    return True
                  else:
                    #self.printD('Problème de connexion')
                    return False
            else:
                #self.printD('Impossible d''ouvrir le port')
                return False
            self.defClose()
        #self.printD('test connection fini')
# LG old        return 'KO'
        return False

    def DesactiverToutesSorties(self):
        try:
            self.printD('Je désactive les sorties')
            self.defOpen()
            self.ecrire('OUTP:SEL OFF')
            for i in range(4):
                self.Channels[i].Desactiver()
                self.printD('Réussie')
                #for i in range (Channels.count):  
                #Desactive les channels et le bouton de sortie (output)
            self.defClose()
        except:
            self.printD('Désactivation de la sortie de courant et de tous les channels impossible')
    

        #Active le bouton de sortie de courant


    def ActiverCourant(self):
        try:
            self.printD('J''allume le courant')
            self.defOpen()
            self.ecrire("OUTP:GEN ON")
            self.defClose()
            return "ok"
        except:
            self.printD('Activation de la sortie de courant impossible')
            return "ko"
        #Active le bouton de sortie de courant

    def DesactiverCourant(self):
        try:
            self.printD('J''allume le courant')
            self.defOpen()
            self.ecrire("OUTP:GEN OFF")
            self.defClose()
            return "ok"
        except:
            self.printD('Désactivation de la sortie du courant impossible')
            return "ko"
        #Désactive le bouton de sortie de courant

    def desactiverSortieChannel(self, channels):
        try:
            self.defOpen()
            self.ecrire("INST OUT"+str(channels))
            self.ecrire("OUTP:SEL OFF")
            self.defClose()
            return "ok"
        except:
            self.printD('Désactivation de tous les channels impossible')
            return "ko"

    def activerSortieChannel(self, channels):
        try:
            self.defOpen()
            self.ecrire("INST OUT"+str(channels))
            self.ecrire("OUTP:SEL ON")
            self.defClose()
            return "ok"
        except:
            self.printD('Désactivation de tous les channels impossible')
            return "ko"
#------------------AJOUT------------------#
    def ActiverUnChannel(self,ch):
        self.defOpen()
        self.ecrire("INST OUT"+str(ch))
        self.ecrire("OUTP:SEL ON")
        self.defClose()
    #
    def DesactiverUnChannel(self,ch):
        self.defOpen()
        self.ecrire("INST OUT"+str(ch))
        self.ecrire("OUTP:SEL OFF")
        self.defClose()
    #
    def LireTensionReelle(self,NumChannel):
        return self.Channels[NumChannel-1].LireTensionReelle()
    #
    def LireIntensiteReelle(self,NumChannel):
        return self.Channels[NumChannel-1].LireIntensiteReelle()
#------------------AJOUT------------------#


    def Reset(self):
        try:
            self.defOpen()
            self.ecrire("*RST")
            self.printD('Je réinitialise la hameg')
            self.defClose()
        except:
            self.printD('Réinitialisation de impossible')
        #Réinitialise complètement la HAMEG

    def VariationAPI(self, noChannel, table):
        try:
            channel = self.Channels[noChannel - 1]
            channel.VariationAPI(table)
        except:
            self.printD('Variation avec tableau impossible')

    def getPorts(self):
        if (self.DEBUG):
            fakePCom = {'name': ['COM5', 'COM4', 'COM3']}
            return fakePCom
        else:

#            #listports = list(port_list.comports())
#            try:
#                listports = list(serial.tools.list_ports.comports())
#                p_dict = None
#                for p in listports:
#                    p_dict = vars(p)
#                self.printD(p_dict['name'])
#                return p_dict
#            except:
#                     #listports = list(port_list.comports())
            '''try:

                listports = list(serial.tools.list_ports.comports())
                p_dict = None
                for p in listports:
                    p_dict = vars(p)
                self.printD(p_dict['name'])
                return p_dict
            except:

                return "Aucun ports"

            # listports = list(serial.tools.list_ports.comports())
            # p_dict = None
            # for p in listports:
            #    p_dict = vars(p)
            # self.printD(p_dict['name'])
            # return p_dict
=======
                return "Aucun ports"'''



            liste = list(port_list.comports())
            listePorts = []
            for p in liste:
                listePorts.append(p.name)
            return listePorts


    def getHamegPorts(self):
        p_dict = self.getPorts()
        self.printD(p_dict)

        #for i in range(len(p_dict['name'])):
        #    self.setCPort(p_dict['name'][i])
        #    #self.printD('test du port = ',p_dict['name'][i] )
        #    if (self.TestConnexion() == True):
        #        self.listPortHameg['pCom'] = p_dict['name'][i]
        #    #self.printD(self.listPortHameg)
        #    i += 1
            #self.printD(f"name: {p_dict['name']}")
        if (self.TestConnexion() == True):
            #self.listPortHameg['pCom'] = p_dict['name'][i]
            return "OK"
        else:
            return "KO"
        #return self.listPortHameg

    def VariationIHM(self, table):
        # Accéder à la clé 'liste' dans l'objet complet
        for i in range(len(table['liste'])):
            element = table['liste'][i]
            print(f"Traitement de l'élément : {element}")
            
            # Utilisation de 'channel' pour sélectionner le bon canal
            channel_index = table['channel'] - 1  # Ajuste en fonction de la logique de tes canaux
            channel = self.Channels[channel_index]
            print(f"Le channel {table['channel']} est valide.")
            
            # Appel à la méthode de variation pour le channel spécifique
            channel.VariationIHM(element)


    def ModifierValeurs(self, noChannel, TensionMax, IntensiteMax ):
        try: 
            channel = self.Channels[noChannel-1]
            channel.ModifierValeurs(TensionMax, IntensiteMax )
        except:
            self.printD('3 Modification de la tension max et de l''intensité max impossible')
        #Modifie les valeurs de la tension max 
        # et de l'intensité max du channel visé

    def ecrireVolt(self, channels, tensions):
#        try:
        #self.defOpen()
        #self.oPort.write(bytes(str + '\n', 'utf-8'))
        self.ecrire("INST OUT" + str(channels))
        #self.ecrire("APPLY " + str(tensions))
        #self.defClose()
        return "OK"
#        except:
#            print('4 Modification de la tension max et de l''intensité max impossible')
#            self.defClose()
#            return "KO"
            # Modifie les valeurs de la tension max
            # et de l'intensité max du channel visé

    def ecrireIntensite(self, channel, intensite):
        try:
            self.defOpen()
            self.ecrire("INST OUT" + str(channel))
            self.ecrire("APPLY " + str(intensite))
            return "OK"
        except:
            print('5 Modification de la tension max et de l''intensité max impossible')
            self.oPort.defClose()
            return "KO"
            # Modifie les valeurs de la tension max
            # et de l'intensité max du channel visé

    def printD(self, string):
        if(self.DEBUG):
            print(string)

    def LireTensionReelle(self, NumChannel):
        try:
            self.defOpen()
            self.ecrire("INST OUT"+str(NumChannel))
            self.ecrire("MEAS:VOLT?")
            return self.lireLigne()
        except:
            print('Lecture de la Tension reelle impossible')
            self.defClose()
        #Retourne la tension en sortie du channel visé

# USELESS USELESS USELESS USELESS USELESS USELESS USELESS USELESS USELESS USELESS USELESS USELESS USELESS USELESS

    def TableVariation(self, Intensite, nbligne):
        try:
            table = [{"Tension": 32, "Intensite": 0, "Duree": 0.5}]
            for i in range(1, nbligne):
                table.append({"Tension": 32, "Intensite": i * Intensite, "Duree": 0.5})
            return table
        except:
            self.printD('Création du tableau impossible')
        # Retourne un tableau avec des les paramètres permettants de faire varier l'intensite sur une duree


    def ToutDesactiverChannel(self):
        try:
            self.defOpen()
            for i in range(0, 4):
                self.ecrire("INST OUT" + str(i + 1))
                self.ecrire("OUTP:SEL OFF")
            self.defClose()
        except:
            self.printD('Désactivation de tous les channels impossible')
        # Desactive et retourne les channels visés


    def ActiverChannel(self):
        try:
            self.defOpen()
            for i in range(0, 4):
                self.ecrire("INST OUT" + str(i + 1))
                self.ecrire("OUTP:SEL ON")
            self.defClose()
            # Active les channels visés
        except:
            self.printD('Activations de tous les channels impossible')

    #methode get port puis methode getportHameg qui utiliserais testconnexion pour tester les ports com il pourrait utiliser setport pour affecter le port
