﻿import sys
import os
import socket
import subprocess
from typing import Container
import serial
from serial import serialwin32
import serial.tools.list_ports as port_list
from serial.win32 import ONE5STOPBITS
import time
import threading
import ast
import time

#encoding = 'utf-8'
#decoding = 'utf-8'
from HMP4040 import HMP4040

lsFichierStop = "C:\\Users\\infosup\\Desktop\\led\\Hameg\\polls\\arreterAnimation.cmd"
liHndl = open(lsFichierStop, "w")
liHndl.write("demande d'arrêt")
liHndl.close()

tableIHM = ast.literal_eval(sys.argv[1])
#print(sys.argv[1])
#portCOM = "COM8"
portCOM = "COM" + str(tableIHM[0]["port-com"])
print("Port com arret : " + portCOM)

lbHamegOK = False
liNbTentatives = 0
while liNbTentatives < 100:
    try:
        print("tentative de prise de hameg")
        liNbTentatives = liNbTentatives + 1
        hameg = HMP4040(portCOM)
        lbHamegOK = True
        print("hameg OK")
    except:
        print('impossible de créer la hameg')
        time.sleep(0.1)
    if lbHamegOK:
        # On a réussi à prendre la hameg
        break


if lbHamegOK:
    # On a réussi à prendre la hameg
    # Désactiver tous les channels
    for liChannel in range(1, 5):
        print("J'éteins le channel" + str(liChannel))
        hameg.ecrire("INST OUT"+str(liChannel))
        hameg.ecrire("APPLY "+str(0)+','+str(0))
        hameg.ecrire("OUTP:SEL OFF")
        time.sleep(0.1)

    # désactiver la sortie ppale
    print("J'éteins la sortie principale")
    hameg.ecrire("OUTP:GEN OFF")
    
    hameg.defClose()
    hameg = None

