﻿import sys
import os
import socket
import subprocess
from typing import Container
import serial
from serial import serial_for_url
import serial.tools.list_ports as port_list
#from serial.win32 import ONE5STOPBITS
import time
import threading
import ast
#import pygame
from threading import Lock

# Verrou global pour accéder aux canaux
encoding = 'utf-8'
decoding = 'utf-8'
from HMP4040 import HMP4040

chemin_complet = os.path.realpath(__file__)
repertoire_parent = os.path.dirname(chemin_complet)

tableIHM = ast.literal_eval(sys.argv[1])

print(sys.argv[1])
# Obsolète : le port COM est maintenant lu dans le JSON de configuration portCom = sys.argv[2]
portCOM = "COM" + tableIHM[0]["port-com"]
print("Port com lancement : " + portCOM)
hameg = HMP4040(sys.argv[2])
hameg.DEBUG = True
#print(type(tableIHM))
#print('Debug')
#print(type(tableIHM))
#if len(tableIHM) > 0 :
#    print("Canal 1 ")
#    print( tableIHM[0])
#if len(tableIHM) > 1 :
#    print("Canal 2 ")
#    print( tableIHM[1])
#if len(tableIHM) > 2 :
#    print("Canal 3 ")
#    print(tableIHM[2])
#if len(tableIHM) > 3 :
#    print("Canal 4 ")
#    print( tableIHM[3])

#print(sys.argv[1])
#print(sys.argv[2])
#tableVariation = hameg.TableVariation(0.001, 100)
#if hameg.TestConnexion():
print('Debut du programme')
    #hameg.ActiverSortie()
#if hameg.TestConnexion():
print(f"Structure de tableIHM avant appel : {tableIHM}")
 




# Lancer la musique et l'effet en parallèle

# Exemple d'appel de la fonction
#  --------------- ANIMATION ---------------------------    
 # Verrou pour synchroniser l'accès au matériel
lock_hameg = threading.Lock()

# Fichier pour détecter une demande d'arrêt
# lsFichierStop = "C:\\Users\\infosup\\Desktop\\led\\Hameg\\polls\\arreterAnimation.cmd"
lsFichierStop = os.path.join(repertoire_parent, "arreterAnimation.cmd")

# Fonction principale pour gérer une variation
def VariationIHM(table, default_channel=None):
    channel = table["channel"] if default_channel is None else default_channel
    intensite_depart = 0

    try:
        # Boucle sur les configurations de la liste
        for config in table["liste"]:
            # Vérifie à chaque configuration si le fichier d'arrêt existe
            if os.path.exists(lsFichierStop):
                print("Demande d'arrêt détectée. Arrêt du canal.")
                break

            intensite_max = (220 / 100) * config["intensite"]
            nb_etape = config["duree"] / 0.2
            pas_intensite = (intensite_max - intensite_depart) / nb_etape

            # Boucle interne pour les étapes
            for step in range(1, int(nb_etape) + 1):
                # Vérifie à chaque étape si le fichier d'arrêt existe
                if os.path.exists(lsFichierStop):
                    print("Demande d'arrêt détectée. Arrêt du canal.")
                    break

                # Piloter l'intensité
                intensite = (intensite_depart + pas_intensite * step) / 1000
                with lock_hameg:
                    hameg.piloter(channel, intensite, 32)

                time.sleep(0.2)  # Pause entre les étapes

            # Mettre à jour l'intensité de départ pour la prochaine config
            intensite_depart = intensite_max

        # Désactiver le canal une fois terminé
        # with lock_hameg:
        #     hameg.piloter(channel, 0, 0)
        #     hameg.DesactiverUnChannel(channel)

    except Exception as e:
        print(f"Erreur sur le canal {channel}: {e}")



# Fonction pour lancer une variation dans un thread
def lancer_variation(element):
    
    VariationIHM(element)

# Fonction pour désactiver tous les canaux après les animations
def desactiver_canaux():
    with lock_hameg:
        for canal in [1, 2, 3, 4]:
            print(f"Désactivation du canal {canal}")
            hameg.piloter(canal, 0, 0)
            hameg.DesactiverUnChannel(canal)

# Supprimer le fichier stop s'il existe au démarrage
# Supprimer le fichier stop s'il existe au démarrage
if os.path.exists(lsFichierStop):
    os.remove(lsFichierStop)

# Activer les canaux nécessaires
for req in tableIHM:
    channel = req["channel"]
    hameg.Channels[int(channel) - 1].Activer()

hameg.ActiverCourant()

# Liste pour suivre les threads
threads = []

try:
    while not os.path.exists(lsFichierStop):  # Continuer tant que le fichier stop n'existe pas
        # Vider la liste des threads pour éviter les doublons
        threads.clear()

        # Démarrer les threads pour chaque canal
        for req in tableIHM:
            channel = req["channel"]
            thread = threading.Thread(target=lancer_variation, args=[req])
            thread.start()
            threads.append(thread)

        # Attendre que tous les threads soient terminés avant de recommencer
        for thread in threads:
            thread.join()

    print("Fichier stop détecté. Arrêt des animations.")
    
finally:
    # Désactiver les canaux après l'animation
    desactiver_canaux()
      
#TestHameg.Test(sys.argv[1])
            
