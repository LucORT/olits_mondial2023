from django.shortcuts import render
from django.http import HttpResponse
import subprocess
from django.utils.datastructures import MultiValueDictKeyError
import time
import ast
import sys
import platform
import os
import json
import serial.tools.list_ports
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse



# Définir le port COM par défaut en fonction du système d'exploitation
if platform.system() == "Windows":
    portComParDefaut = "4"
    portSysteme="COM"
elif platform.system() == "Linux":
    portComParDefaut = "0"
    portSysteme = "/dev/ttyUSB"

# Chemin absolu du script actuel
chemin_complet = os.path.realpath(__file__)
repertoire_parent = os.path.dirname(chemin_complet)

# Définir le répertoire de travail pour l'application (utiliser os.path.join pour gérer les séparateurs)
chemin = repertoire_parent  # Répertoire où se trouvent les scripts

# Ajouter le répertoire de travail à sys.path si nécessaire
sys.path.append(chemin)

# Importer des modules si nécessaire
from HMP4040 import HMP4040

# Fonction pour vérifier si le port série est disponible
def is_port_available(port_name):
    ports = serial.tools.list_ports.comports()
    return any(port_name == port.device for port in ports)

# Fonction principale de la vue Django
def index(request):
    # Vérifie si 'dataHameg' est fourni dans la requête, sinon utilise des données par défaut
    if 'dataHameg' not in request.POST:
        file = '[' + \
               '{"channel":1,"port-com":"' + portComParDefaut + '","liste":[{"intensite": 80, "duree": 5},{"intensite": 20, "duree": 7},{"intensite": 70, "duree": 10}]},' + \
               '{"channel":2,"port-com":"' + portComParDefaut + '","liste":[{"intensite":80,"duree":5},{"intensite":20,"duree":7},{"intensite":70,"duree":10}]},' + \
                '{"channel":3,"port-com":"' + portComParDefaut + '","liste":[{"intensite":80,"duree":5},{"intensite":20,"duree":7},{"intensite":70,"duree":10}]},' + \
               '{"channel":4,"port-com":"' + portComParDefaut + '","liste":[{"intensite": 80, "duree": 5},{"intensite": 20, "duree": 7},{"intensite": 70, "duree": 10}]}' + \
               ']'
    else:
        file = request.POST['dataHameg']
    
    # Extraire le port des données reçues au format JSON
    data = json.loads(file)
    full_port = portSysteme + data[0]['port-com']

    print('---LANCEMENT---------------------------------------------------------')
    print(file)
    print(chemin)
    
    # Vérifie si le port est disponible avant de lancer le processus
    if is_port_available(full_port):
        subprocess.Popen(['python', os.path.join(chemin, 'lancerAnimation.py'), file, full_port])
    else:
        print(f"Erreur : Le port {full_port} n'est pas disponible ou n'est pas connecté.")
        return HttpResponse(f"Erreur : Le port {full_port} n'est pas disponible ou n'est pas connecté.", status=500)

    return HttpResponse(file)

# Fonction pour arrêter l'animation
def arreterAnimation(request):
    # if 'dataHameg' not in request.POST:
    #     file = '[' + \
    #            '{"channel":1,"port-com":"' + portComParDefaut + '","liste":[{"intensite":80,"duree":5},{"intensite":20,"duree":7},{"intensite":70,"duree":8}]},' + \
    #            '{"channel":2,"port-com":"' + portComParDefaut + '","liste":[{"intensite":50,"duree":4},{"intensite":40,"duree":8},{"intensite":15,"duree":10}]},' + \
    #            '{"channel":3,"port-com":"' + portComParDefaut + '","liste":[{"intensite":80,"duree":5},{"intensite":20,"duree":7},{"intensite":70,"duree":8}]},' + \
    #            '{"channel":4,"port-com":"' + portComParDefaut + '","liste":[{"intensite":50,"duree":4},{"intensite":40,"duree":8},{"intensite":15,"duree":10}]}' + \
    #            ']'
    # else:
    #     file = request.POST['dataHameg']

    # print('--ARRET--------------------------------------------------------------')
    # print(file)
    
    # # Lancer l'animation d'arrêt via un subprocess
    # subprocess.Popen(['python', os.path.join(chemin, 'arreterAnimation.py'), file])

    # # Traiter les données et attendre que le périphérique soit libre
    # tableIHM = ast.literal_eval(file)
    # portCOM = "COM" + str(tableIHM[0]["port-com"]) if platform.system() == "Windows" else "/dev/ttyUSB" + str(tableIHM[0]["port-com"])
    # print("Port com arrêt : " + portCOM)
    
    # # Tentatives pour accéder à Hameg et libérer le port
    # liNbTentatives = 0
    # lbHamegOK = False
    # while liNbTentatives < 100:
    #     try:
    #         liNbTentatives += 1
    #         hameg = HMP4040(portCOM)
    #         lbHamegOK = True
    #         print("Hameg OK")
    #     except:
    #         print('Impossible de créer la connexion Hameg')
    #         time.sleep(0.1)
        
    #     if lbHamegOK:
    #         # Libérer la connexion à Hameg
    #         hameg.defClose()
    #         hameg = None
    #         print('Hameg libre')
    #         break
    # Générer le contenu pour le fichier arreterAnimation.cmd
    print('--ARRET--------------------------------------------------------------')
    # Générer le fichier `arreterAnimation.cmd`
    chemin_fichier_cmd = os.path.join(chemin, 'arreterAnimation.cmd')

    try:
        with open(chemin_fichier_cmd, 'w') as fichier_cmd:
            fichier_cmd.write(f"Arrêt demandé\n")
        print(f"Fichier d'arrêt créé : {chemin_fichier_cmd}")
    except Exception as e:
        print(f"Erreur lors de la création du fichier d'arrêt : {e}")
        return HttpResponse(f"Erreur : {e}", status=500)
    
    print('--FIN ARRET--------------------------------------------------------------')
    
    return HttpResponse(f"Animation arrêtée. Fichier créé : {chemin_fichier_cmd}")

  
    # return HttpResponse(file)
