# Lancer le serveur Django des raspberry esclave du projet MondialDesMetiers
# Ce script est lancé au démarrage de la raspberry par le service défini dans /etc/systemd/system/server-ihm.service
# (qui devrait s'appeller server-esclave.service)
# cd usr/bin/olits_mondial2023/raspberryEsclave/home/master/myproject
# cd /usr/bin/olits_mondial2023/raspberryEsclave/djangotest
# python3 -m pip install -r requirements.txt
# source "/home/master/myenv/bin/activate"
# cd bin/myenv/
source /bin/olits_mondial2023/myenv/bin/activate
# sudo python3 manage.py runserver 0.0.0.0:8000
cd /usr/bin/olits_mondial2023/Hameg
python manage.py runserver 0.0.0.0:8000



# sur poste de LG
# cd "/home/luc/partage_win10/Luc/Lycée ORT/C2SI/2023-2024 3OLEN/Initiaux/Mondial2023/projetmondialdesmetiers_2023_LucORT/raspberryEsclave/djangotest"
# python3 manage.py runserver 0.0.0.0:8088
# ou /usr/bin/python3 "/home/luc/partage_win10/Luc/Lycée ORT/C2SI/2023-2024 3OLEN/Initiaux/Mondial2023/projetmondialdesmetiers_2023_LucORT/raspberryEsclave/djangotest/manage.py" runserver 0.0.0.0:8088
