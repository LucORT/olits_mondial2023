# script d'automatisation du pull des dernières versions de la branche "recette"

cd ~/git-test/olits_mondial2023

# Annulation des modifications des ajouts de fichiers ou dossier
git clean -fd 

# Annulation des modifications écrite dans un fichier
git reset --hard

# Bascule dans la branche "recette" 
git checkout recette

# tirer les modifications
git pull

# TODO : donner les droits d'écriture sur les dossiers qui perdent ce droit après el pull (exemple : django ne peut plus accéder à ses fichiers de log)
# TODO : de faire un crontab -e pour planifier une tâche pour lancer ce script
# TODO : appeller ce script depuis l'IHM maitre
