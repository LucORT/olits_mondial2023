from flask import Flask, render_template, Response
import cv2
from picamera2 import Picamera2
import time

app = Flask(__name__)

picam2 = Picamera2()
camera_config = picam2.create_preview_configuration(main={"size": (640, 480)})
picam2.configure(camera_config)

time.sleep(2)

picam2.start()

time.sleep(2)

def generate():
	while True:
		frame = cam.capture_array()
		cv2.rectangle(frame, (50, 50), (200, 200), (0, 0, 255), 2)

		_, jpeg = cv2.imencode('.jpg', frame)
		frame_data = jpeg.tobytes()

		yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + frame_data + b'\r\n\r\n')

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/video_feed')
def video_feed():
	return Response(generate(), mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
	app.run(host='192.168.10.30', port=8000, debug=True)
	picam2.stop()
