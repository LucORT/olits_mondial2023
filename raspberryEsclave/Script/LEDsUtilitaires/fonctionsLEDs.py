# Fonctions et constantes pour faciliter le gestion des animations des LEDs
import time
import io
from pathlib import Path
import os

# Déterminer si on est sur une Raspberry
# d'après https://raspberrypi.stackexchange.com/questions/5100/detect-that-a-python-program-is-running-on-the-pi
# LG 20241129
def is_raspberrypi():
    try:
        with io.open('/sys/firmware/devicetree/base/model', 'r') as m:
            if 'raspberry pi' in m.read().lower(): return True
    except Exception: pass
    return False

# LG 20241129 old if False:
if  is_raspberrypi():
    # pour utilisation de la bibliothèque réelle de pilotage GPIO des Raspberry
    from rpi_ws281x import *
else:
    # Pour utilisation du mock de pilotage GPIO des Raspberry et les tests préliminaires sur un autre OS
    from LEDsUtilitaires.mock_rpi_ws281x import *

# ------------------------------------------------------------------------------
# Constantes utiles
# LG 20241129 : déplacé de variables.py
LED_COUNT      = 60
LED_PIN        = 18
LED_FREQ_HZ    = 800000
LED_DMA        = 10
LED_BRIGHTNESS = 255
LED_INVERT     = False
LED_CHANNEL    = 0

# LG 20241129 : déplacé de Noel.py
# LG 20241129 début
# NOIR = Color(0, 0, 0)
# ROUGE = Color(255, 0, 0)
# VERT = Color(0, 255, 0)
# BLEU = Color(0, 0, 255)
# JAUNE = Color(255, 255, 0)
# CYAN = Color(0, 255, 255)
# MAGENTA = Color(255, 0, 255)
# BLANC = Color(255, 255, 255)

COULEURS = {}
COULEURS["NOIR"] = Color(0, 0, 0)
COULEURS["ROUGE"] = Color(255, 0, 0)
COULEURS["VERT"] = Color(0, 255, 0)
COULEURS["BLEU"] = Color(0, 0, 255)
COULEURS["JAUNE"] = Color(255, 255, 0)
COULEURS["CYAN"] = Color(0, 255, 255)
COULEURS["MAGENTA"] = Color(255, 0, 255)
COULEURS["BLANC"] = Color(255, 255, 255)

NOIR = COULEURS["NOIR"]
ROUGE = COULEURS["ROUGE"]
VERT = COULEURS["VERT"]
BLEU = COULEURS["BLEU"]
JAUNE = COULEURS["JAUNE"]
CYAN = COULEURS["CYAN"]
MAGENTA = COULEURS["MAGENTA"]
BLANC = COULEURS["BLANC"]

# Trouver le nom de la couleur (si la couleur est une couleur nommée)
# LG 20241129
def LEDs_getColorName(color):
    leNom = COULEURS.keys()[COULEURS.values().index(color)]
    if not leNom:
        leNom = color
    return leNom
# LG 20241129 fin

# ------------------------------------------------------------------------------
# Fonctions utiles

# Colorer une LED
# strip : valeur de retour de la fonction d'initialisation du ruban : Adafruit_NeoPixel
# couleur : couleur obtenue par Color()
# led : le N° de led à allumer (entre 0 et LED_COUNT)
def colorisation(strip, couleur, led):
	strip.setPixelColor(led, couleur)
	strip.show()

# Colorier toutes les LEDs du ruban, avec un effet progressif
# strip     : valeur de retour de la fonction d'initialisation du ruban : Adafruit_NeoPixel
# couleur   : couleur obtenue par Color()
# wait_ms   : délai entre deux allumages de leds 
# LG 20241129 old def colorWipe(strip, color, wait_ms=50):
def colorWipe(strip, color, wait_ms=10/500.0):
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
# LG 20241129 old        time.sleep(10/500.0)
        time.sleep(wait_ms)

# Colorier toutes les LEDs du ruban, immediatement
# strip     : valeur de retour de la fonction d'initialisation du ruban : Adafruit_NeoPixel
# couleur   : couleur obtenue par Color()
# LG 20241129
def colorImmediat(strip, color):
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)

    strip.show()

def etudeTrame(strip, donnees):
    i = 0
    led = 0
    while(i < len(donnees)):
        color = Color(donnees[i], donnees[i + 1], donnees[i + 2])
        colorisation(strip, color, led)
        i = i + 3
        led = led + 1

# Déterminer le fichier qui sert pour la fin de boucle
# LG 20241129
def LEDs_getFichierBoucle():
    BASE_DIR = Path(__file__).resolve().parent.parent
    fichierFinBoucle = str(BASE_DIR) + '/finBoucle.sh'
    return fichierFinBoucle

# Créer le fichier qui sert pour la fin de boucle
# LG 20241129
def LEDs_poseFichierBoucle():
    fichierFinBoucle = LEDs_getFichierBoucle()
    with open(fichierFinBoucle, "w"):
        pass

# Déterminer si le fichier qui sert pour la fin de boucle existe
# LG 20241129
def LEDs_existeFichierBoucle():
    fichierFinBoucle = LEDs_getFichierBoucle()
    return bool(os.path.exists(fichierFinBoucle))

# Supprimer le fichier qui sert pour la fin de boucle
# LG 20241129
def LEDs_effaceFichierBoucle():
    fichierFinBoucle = LEDs_getFichierBoucle()
    if os.path.exists(fichierFinBoucle):
        os.remove(fichierFinBoucle)
        message = "Fichier de boucle supprimé avec succès : arrêt demandé"
    else:
        message = "Le fichier de boucle n'existe pas : l'arrêt avait déjà été demandé"
    
    return message
