# Mock pour faire semblant d'utiliser rpi_ws281x quand on n'est pas sur une raspberry
# Benjamin Mazard
# from LEDsUtilitaires.variables import *   # LG 20241129

def Color(red : int,green : int,blue :int) -> str :
# LG 20241129 old    return f"({red},{green},{blue})"
    return f"RGB({red},{green},{blue})"

def Adafruit_NeoPixel(count : int,pin : int,freq_hz : int,dma : int,invert : bool,brightness :int,channel :int) -> str:
# LG 20241129 old    return f"({count},{pin},{freq_hz},{dma},{invert},{brightness},{channel})"
    return mockStrip(count, pin, freq_hz, dma, invert, brightness, channel)

class mockStrip:
    count=None
    pin=None
    freq_hz=None
    dma=None
    invert=None
    brightness=None
    channel=None
    
    LEDs = {}
    def __init__(self, count, pin, freq_hz, dma, invert, brightness, channel) -> None:
        self.count = count
        self.pin = pin
        self.freq_hz = freq_hz
        self.dma = dma
        self.invert = invert
        self.brightness = brightness
        self.channel = channel

    def begin(self):
        pass

    def numPixels(self):
        return self.count

    def setPixelColor(self, noLED, color):
        if noLED < 0:
            pass
        self.LEDs[str(noLED)] = color
        
    def show(self):
        # Construire le texte d'état des leds
        etatLEDs = ""
        etatLedPrec = None
        etatToutesLEDs = None
        for led in self.LEDs:
            couleur = self.LEDs[led]
#            couleur = LEDs_getColorName(couleur)

            if etatLEDs:
                etatLEDs += ", "
# LG 20241129 old            etatLEDs += led + ":" + self.LEDs[led]
            etatLEDs += led + ":" + couleur

            if not etatLedPrec:
                # Première LED
# LG 20241129 old                etatLedPrec = self.LEDs[led]
                etatLedPrec = couleur
# LG 20241129 old                etatToutesLEDs = self.LEDs[led]
                etatToutesLEDs = couleur
# LG 20241129 old            if not etatLedPrec == self.LEDs[led]:
            if not etatLedPrec == couleur:
                # Cette LED n'est pas dans le même état que la précédente : on ne pourra pas simplifier
                etatToutesLEDs = None
                
        if len(self.LEDs) == self.count and etatToutesLEDs:
            # Toutes les LEDs sont dans le même état : simplifier l'affichage
            etatLEDs = f"toutes à {etatToutesLEDs}"

        print(f"Allumage des leds : {etatLEDs}")        
