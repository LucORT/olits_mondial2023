# Affiche toutes les leds en noir (ruban éteint)
# LG 20241129 déac from sys import path

# LG 20241129 old from ArtNet.fonctions import *      # fait l'import de "from rpi_ws281x import *" + "import time"
from LEDsUtilitaires.fonctionsLEDs import *      # fait l'import de "from rpi_ws281x import *" + "import time"
# LG 20241129 déac from ArtNet.variables import *

# LG 20241129 déac path.append('../../../Fichier_commun')
# LG 20241129 déac path.append('/home/pi/polar-led-project/djangotest/Fichier_commun')

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()

# LG 20241129 début
# try:
#     colorWipe(strip, Color(0,0,0))
        
# except KeyboardInterrupt:
#     colorWipe(strip, Color(0,0,0))
colorImmediat(strip, NOIR)
# LG 20241129 fin