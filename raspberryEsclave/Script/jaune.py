# LG 20241129 déac import os
import os.path
# LG 20241129 déac from sys import path

# LG 20241129 old from ArtNet.fonctions import *      # fait l'import de "from rpi_ws281x import *" + "import time"
from LEDsUtilitaires.fonctionsLEDs import *      # fait l'import de "from rpi_ws281x import *" + "import time"
# LG 20241129 déac : tout a été mis dans fonctions.py from ArtNet.variables import *

# LG 20241129 déac from pathlib import Path #eddy

# Déterminer et créer le fichier qui sert pour la fin de boucle
# LG 20241129 début
# # LG 20241128 old BASE_DIR = Path(__file__).resolve().parent.parent
# BASE_DIR = Path(__file__).resolve().parent
# fichierFinBoucle = str(BASE_DIR) + '/finBoucle.sh'
# q = open(fichierFinBoucle, "w")
# q.close()
LEDs_poseFichierBoucle()
# LG 20241129 fin

#path.append('../../../Fichier_commun')
#path.append('/home/pi/polar-led-project/djangotest/Fichier_commun')
#mise en commentaire que ces liens sont faut et pas utiliser 

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()

try:
    while True:
# LG 20241129 old        colorWipe(strip, Color(255,255,255))
        colorWipe(strip, JAUNE)
# LG 20241128 old        if not bool(os.path.exists(str(BASE_DIR) + '/djangotest/finBoucle.sh')): 
# LG 20241129 old        if not bool(os.path.exists(fichierFinBoucle)): 
        if not LEDs_existeFichierBoucle() : 
            # Le fichier qui sert pour la fin de boucle n'existe plus : arrêter l'animation
            break
            
    # A la sortie de la boucle, on remet tout en noir
# LG 20241129 old    colorWipe(strip, Color(0,0,0))
    colorWipe(strip, NOIR)

except KeyboardInterrupt:
# LG 20241129 old    colorWipe(strip, Color(0,0,0))
    colorWipe(strip, NOIR)