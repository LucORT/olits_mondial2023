import os
import os.path
# LG 20241127 déac import time
import argparse

# LG 20241127 début
# from rpi_ws281x import *

# LG 20241129 déac : tout a été mis dans fonctions.py from ArtNet.variables import *
# LED_COUNT      = 60    # Number of LED pixels.
# LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
# #LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
# LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
# LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
# LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
# LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
# LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
# LG 20241129 déac début : variables déplacées dans ArtNet.variables
# NOIR = Color(0, 0, 0)
# ROUGE = Color(255, 0, 0)
# VERT = Color(0, 255, 0)
# BLEU = Color(0, 0, 255)
# JAUNE = Color(255, 255, 0)
# CYAN = Color(0, 255, 255)
# MAGENTA = Color(255, 0, 255)
# BLANC = Color(255, 255, 255)

# # LG 20241129 old from ArtNet.fonctions import *      # fait l'import de "from rpi_ws281x import *" + "import time"
from LEDsUtilitaires.fonctionsLEDs import *      # fait l'import de "from rpi_ws281x import *" + "import time"

# # Déterminer et créer le fichier qui sert pour la fin de boucle
# from pathlib import Path #eddy
# BASE_DIR = Path(__file__).resolve().parent
# fichierFinBoucle = str(BASE_DIR) + '/finBoucle.sh'
# q = open(fichierFinBoucle, "w")
# q.close()
# # print(fichierFinBoucle)
# # exit(0)
LEDs_poseFichierBoucle()
# LG 20241127 fin


# LG 20241127 old def test(strip, color, wait_ms = 50):
# La fonction renvoie faux quand un arrêt a été détecté
def animationNoel(strip, color, wait_ms = 50):
    arreter = False     # LG 20241127
    for i in range(strip.numPixels()//2):
# LG 20241127 début
# LG 20241129 old        if not bool(os.path.exists(fichierFinBoucle)):
        if not LEDs_existeFichierBoucle():
            # Le fichier qui sert pour la fin de boucle n'existe plus : arrêter l'animation
            arreter = True
            break
# LG 20241127 fin
        strip.setPixelColor(i*2, color)
        strip.show()
        time.sleep(wait_ms/1000.0)
# LG 20241127 début
    if arreter:
        # Le fichier qui sert pour la fin de boucle n'existe plus : arrêter l'animation
        return False
# LG 20241127 fin

    for i in range(strip.numPixels()//2):
# LG 20241129 début
#        strip.setPixelColor(i*2-1, color)
        noLED = i*2-1
        if noLED < 0 or noLED > LED_COUNT :
            continue
        strip.setPixelColor(noLED, color)
# LG 20241129 fin
        strip.show()
        time.sleep(wait_ms/1000.0)
# LG 20241127 début
# LG 20241129 old        if not bool(os.path.exists(fichierFinBoucle)):
        if not LEDs_existeFichierBoucle():
            # Le fichier qui sert pour la fin de boucle n'existe plus : arrêter l'animation
            arreter = True
            break

    return not arreter
# LG 20241127 fin
    
if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    print(strip)
    strip.begin()

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:

# LG 20241127 début
        """
        while True:
            test(strip, ROUGE)
            if not bool(os.path.exists(fichierFinBoucle)):
                print("le fichier n'existe plus")
                test(strip, NOIR)
                break
            test(strip, BLANC)
            if not bool(os.path.exists(fichierFinBoucle)):
                print("le fichier n'existe plus")
                test(strip, NOIR)
                break
            test(strip, BLEU)
            if not bool(os.path.exists(fichierFinBoucle)):
                print("le fichier n'existe plus")
                test(strip, NOIR)
                break
            test(strip, BLANC)
            if not bool(os.path.exists(fichierFinBoucle)):
                print("le fichier n'existe plus")
                test(strip, NOIR)
                break
            test(strip, JAUNE)
            if not bool(os.path.exists(fichierFinBoucle)):
                print("le fichier n'existe plus")
                test(strip, NOIR)
                break
            test(strip, BLANC)
            if not bool(os.path.exists(fichierFinBoucle)):
                print("le fichier n'existe plus")
                test(strip, NOIR)
                break
            test(strip, ROUGE)
            if not bool(os.path.exists(fichierFinBoucle)):
                print("le fichier n'existe plus")
                test(strip, NOIR)
                break
            """
        while True:
            if not animationNoel(strip, ROUGE):
                break
            if not animationNoel(strip, BLANC):
                break
            if not animationNoel(strip, BLEU):
                break
            if not animationNoel(strip, BLANC):
                break
            if not animationNoel(strip, JAUNE):
                break
            if not animationNoel(strip, BLANC):
                break
            if not animationNoel(strip, ROUGE):
                break
            
        # A la sortie de la boucle, on remet tout en noir
        colorWipe(strip, NOIR)
# LG 20241127 fin

    except KeyboardInterrupt:
        # Arrêt du processus par Ctrl+C (mode lancement manuel, inopérant quand c'est Django qui a appellé le script)
        if args.clear:
            # Le script a été appellé avec l'arguent "-c" : eteindre les leds
            animationNoel(strip, NOIR, 10)
