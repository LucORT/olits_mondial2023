#!/usr/bin/python
# -*- coding: utf8 -*-
from functions import *      # fait l'import de "from rpi_ws281x import *"
from constante import *
import time

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()

try:
    while True:
        #boucle qui permet de coloré le ruban en blanc
        colorWipe(strip, Color(255,255,255)) #couleur blanche
        time.sleep(1) #timeur de 5 secondes pour le teste du ruban led
        colorWipe(strip, Color(255,255,155)) #couleur blanche
        time.sleep(1) #timeur de 5 secondes pour le teste du ruban led
        colorWipe(strip, Color(255,255,100)) #couleur blanche
        time.sleep(1) #timeur de 5 secondes pour le teste du ruban led
        colorWipe(strip, Color(255,155,255)) #après le timeur extinction des led
        time.sleep(1) #timeur de 5 secondes pour le teste du ruban led
        colorWipe(strip, Color(155,155,0)) #après le timeur extinction des led
        time.sleep(1) #timeur de 5 secondes pour le teste du ruban led
        colorWipe(strip, Color(155,255,0)) #après le timeur extinction des led
        time.sleep(2) #timeur de 5 secondes pour le teste du ruban led
        colorWipe(strip, Color(0,0,0)) #après le timeur extinction des led
        break #sort de la boucle


except KeyboardInterrupt: #si il y a une erreur alors ne pas démarré les led
    colorWipe(strip, Color(0,0,0))
