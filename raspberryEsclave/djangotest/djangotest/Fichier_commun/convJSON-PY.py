import json
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse
import requests

import constante as const
import functions      # fait l'import de "from rpi_ws281x import *"

ipurl = functions.getIp()
print(ipurl)

url = ipurl + ":8000"

response = requests.post(url)
print(response)


# -------------------------- Récupération contenu fichier json
with open(const.DATA, 'r') as file :
    data = json.load(file)

#data['ip'] = IP
#data['univers'] = univers

# -------------------------- Ecriture dans le fichier json
with open(const.DATA, 'w') as file:
    json.dump(data, file, indent=4)


    # x = {
    # "ip": str(IP, 'UTF-8'),
    # "univers": str(univers),
    # }

    # Conversion en JSON avec les indentations
    # y = json.dumps(x, indent=4)

    # # Le résultat est une chaîne de caractères:
    # print("Conversion Python -> JSON")
    # print(y)


    # with open("/home/pi/polar_led/djangotest/Fichier_commun/data.json", mode='w') as file:
    #     # file.write(y)

    # # -------------------- Conversion JSON -> Python

    # with open("/home/pi/polar_led/djangotest/Fichier_commun/data.json", mode='r') as file:
    #     res = file.read()
    #     z = json.loads(res)

    # # print("Conversion JSON -> Python")
    # print(z["ip"])
    # print(z["univers"])