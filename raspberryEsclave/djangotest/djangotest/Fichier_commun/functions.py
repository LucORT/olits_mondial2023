# coding: utf-8
# Module de la librairie standard de Python (aucune installation supplémentaire requise)
from json import dump, load
from subprocess import check_output
from sys import path
from time import sleep
from os import system
import socket
import re

from rpi_ws281x import *

# Permet d'ajouter le chemin de l'emplacement des fichiers communs
# LG 20240530 début
# path.append('../../../Fichier_commun')
# path.append('/home/pi/polar-led-project/djangotest/djangotest/Fichier_commun')
from pathlib import Path
path.append(str(Path(__file__).resolve().parent))
# LG 20240530 fin

# Import des fichiers personelles
# Fichier contenant toute les constantes utilisées par le programme (.../polar-led-project/Fichier_communs/constante.py)
import constante as const

# -----------------------------------Fonctions ECRAN LCD -----------------------------------------------------

def line(numLine):
    """Fonction permettant de choisir la ligne de l'écran sur laquelle on souhaite écrire

    Args:
        numLine (integer): Numero de la ligne (max = 5)

    Returns:
        integer: Retourne le résultat du calcul effectué par la fonction
    """
    return const.PADDINGTOP + const.LINEHEIGHT * numLine

def menu(draw_object):
    """Permet d'afficher l'IHM en bas de l'écran qui définit le nom de chaque bouton.

    Args:
        draw_object (class ImageDraw): Nom de l'objet de dessin qui est utilisé sur la page
    """    
    # Affichage de l'IHM en bas de l'écran avec les différents noms des boutons.
    draw_object.line((const.XABSCISSA, const.PADDINGTOP+52, const.XABSCISSA+128, const.PADDINGTOP+52), fill=255)
    draw_object.text((const.XABSCISSA, const.PADDINGTOP+54), 'Menu', font=const.FONT, fill=255)
    draw_object.text((const.XABSCISSA+32, const.PADDINGTOP+54), 'Entrer', font=const.FONT, fill=255)
    draw_object.text((const.XABSCISSA+76, const.PADDINGTOP+54), 'Haut', font=const.FONT, fill=255)
    draw_object.text((const.XABSCISSA+110, const.PADDINGTOP+54), 'Bas', font=const.FONT, fill=255)

def getIp():
    """Récupère l'adresse IP du Raspberry pour la stocker dans une variable
    """
    # Commande permettant de récupérer l'adresse IP
    cmd = "hostname -I | cut -d\' \' -f1"
    # Stockage du résultat 
    res = check_output(cmd, shell = True)
    # Formatage du résultat
    ip = str(res, 'UTF-8')
    return ip

def formatUnivers(draw_object, listUnivers):
    """Fonction permettant d'afficher l'univers avec un format correct

    Args:
        draw_object (class ImageDraw): Nom de l'objet de dessin qui est utilisé sur la page
        listUnivers (list[integer]): Liste contenant les 5 chiffres de l'univers sous la forme [x, y, z, a, b]
    """
    # Conversion de la liste d'integer en string afin de pouvoir les afficher
    univers = ''.join(str(elem) for elem in listUnivers)

    # Affichage du nouvel univers.
    draw_object.text((const.XABSCISSA, line(0)), univers, font=const.FONT, fill=255)

def formatIP(draw_object, oct1List, oct2List, oct3List, oct4List):
    """Fonction permettant d'afficher l'adresse IP avec un format correct.

    Args:
        draw_object (class ImageDraw): Nom de l'objet de dessin qui est utilisé sur la page
        oct1List (list[integer]): Liste contenant les valeurs du 1er octet sous la forme octNList = [x, y, z]
        oct2List (list[integer]): Liste contenant les valeurs du 2e octet sous la forme octNList = [x, y, z]
        oct3List (list[integer]): Liste contenant les valeurs du 3e octet sous la forme octNList = [x, y, z]
        oct4List (list[integer]): Liste contenant les valeurs du 4e octet sous la forme octNList = [x, y, z]
    """ 
    # Conversion des listes d'integer en string afin de pouvoir les afficher.   
    oct1 = ''.join(str(elem) for elem in oct1List)
    oct2 = ''.join(str(elem) for elem in oct2List)
    oct3 = ''.join(str(elem) for elem in oct3List)
    oct4 = ''.join(str(elem) for elem in oct4List)

    # Fusion des conversions des différentes listes.
    addr = str(oct1) + '.' + str(oct2) + '.' + str(oct3) + '.' + str(oct4)
    # Affichage de la nouvelle adresse IP.
    draw_object.text((const.XABSCISSA, line(0)), addr, font=const.FONT, fill=255)

def affValSet(draw_object, varNum, position):
    """Fonction permettant d'afficher la nouvelle valeur du champ sélectionné.

    Args:
        draw_object (class ImageDraw): Nom de l'objet de dessin qui est utilisé sur la page
        varNum (integer): Valeur qui doit être modifié
        position (integer): Champ affecter à la valeur
    """ 
    # Affichage du champ avec sa nouvelle valeur  
    draw_object.text((const.XABSCISSA,line(1)), 'Champ : ' + str(position), font=const.FONT, fill=255)
    draw_object.text((const.XABSCISSA,line(2)), 'Nouvelle valeur : ' + str(varNum), font=const.FONT, fill=255)
    draw_object.text((const.XABSCISSA,line(4)), '"Entrer" pour valider', font=const.FONT, fill=255)

def valideIpIhm(ip):

    with open('/home/pi/polar-led-project/interfaces', 'w+') as file:
        file.write(const.HEAD + "\nauto eth0 \niface eth0 inet static \naddress " + ip + '\nnetmask 24')
    
    system('sudo sh /home/pi/polar-led-project/changeIP.sh')

def valideIP(oct1List, oct2List, oct3List, oct4List):
    """Fonction permettant de valider les modifications effectuées lors du changement de l'adresse IP.

    Args:
        oct1List (list[integer]): Liste contenant les valeurs du 1er octet sous la forme octNList = [x, y, z]
        oct2List (list[integer]): Liste contenant les valeurs du 2e octet sous la forme octNList = [x, y, z]
        oct3List (list[integer]): Liste contenant les valeurs du 3e octet sous la forme octNList = [x, y, z]
        oct4List (list[integer]): Liste contenant les valeurs du 4e octet sous la forme octNList = [x, y, z]
    """ 

    oct1 = remove0(oct1List)
    oct2 = remove0(oct2List)
    oct3 = remove0(oct3List)
    oct4 = remove0(oct4List)

    # Fusion des conversions des différentes listes.
    addr = str(oct1) + '.' + str(oct2) + '.' + str(oct3) + '.' + str(oct4)

    # Ouverture du fichier de configuration d'interface et ajout à la fin de la nouvelle adresse IP.
    with open('/home/pi/polar-led-project/interfaces', 'w+') as file:
        file.write(const.HEAD + "\nauto eth0 \niface eth0 inet static \naddress " + addr + '\n')

    with open(const.DATA, 'r') as file:
            data = load(file)

    # Attribution d'une valeur au champ "ip"
    data["ip"] = addr

    # Copie des modifications dans le fichier
    with open(const.DATA, 'w+') as file:
        dump(data, file, indent=4)

def remove0(octList):
    """Fonction permettant de supprimer les 0 inutiles présents lors de la validation de l'adresse IP.

    Args:
        octList (list[integer]): Liste contenant l'octet avec des 0 inutiles

    Returns:
        string : Retourne l'octet passé en paramètre sans les 0 inutiles
    """
    octString = ''.join(str(elem) for elem in octList)

    if octString == '000':
        octListFormat = '0'

    else:
        octListFormat = re.sub("^[0]*", '', octString)
    
    return octListFormat

def valideUnivers(listUnivers):
    """Fonction permettant de valider les modifications apportées à l'univers

    Args:
        listUnivers (list[integer]): Liste contenant les 5 chiffres \
            de l'univers sous la forme [x, y, z, a, b]
    """
    # Enlève les 0 inutiles
    universFormat = remove0(listUnivers)

    # Conversion de la liste d'integer en string afin de pouvoir les afficher
    univers = ''.join(str(elem) for elem in universFormat)
    
    # Récupération du contenu du fichier data.json
    with open(const.DATA, 'r') as file:
        data = load(file)

    # Attribution de la valeur souhaité au champ univers
    data["univers"] = univers

    # Ecriture de la valeur renseigné dans le champ
    with open(const.DATA, 'w+') as file:
        dump(data, file, indent=4)

# -----------------------------------Fonctions RUBAN LED -----------------------------------------------------

def colorisation(strip, couleur, led):
    """Allume une seule LED en fonction des valeurs passées en paramètre

    Args:
        strip (Adafruit_NeoPixel): Objet instancié représentant le ruban LED \
            prennant 7 paramètres : \
                LED_COUNT : Nombre total de LED disponible sur le ruban\
                LED_PIN : Correspond au numéro du PIN utilisé pour transmettre les \
                    données (GPIO 18)
                LED_FREQ_HZ : Fréquence des LED en Hertz
                LED_DMA : Canal utilisé pour générer le signal
                LED_BRIGHTNESS : Intensité des LED (0 = sombre, 255 = lumineux)
                LED_INVERT : Inverse le signal (True pour inversé)
                LED_CHANNEL : Mettre à '1' pour les GPIOs 13, 19, 41, 45 ou 53
        couleur (Color): Objet prenant 3 paramètres correspondant à l'intensité \ 
            de chaque LED. Les 3 trois valeurs doivent être comprises entre 0 et 255
        led (integer): Représente le numéro de la LED à allumer. Il doit être compris \
            entre 0 et le nombre de LED total passé en paramètre lors de la création de \
            l'objet strip
    """
    strip.setPixelColor(led, couleur)
    strip.show()

def colorWipe(strip, couleur, wait_ms=50):
    """Allume toutes les LED du ruban LED passé en paramètre

    Args:
        strip (Adafruit_NeoPixel): Objet instancié représentant le ruban LED \
            prennant 7 paramètres : \
                LED_COUNT : Nombre total de LED disponible sur le ruban\
                LED_PIN : Correspond au numéro du PIN utilisé pour transmettre les \
                    données (GPIO 18)
                LED_FREQ_HZ : Fréquence des LED en Hertz
                LED_DMA : Canal utilisé pour générer le signal
                LED_BRIGHTNESS : Intensité des LED (0 = sombre, 255 = lumineux)
                LED_INVERT : Inverse le signal (True pour inversé)
                LED_CHANNEL : Mettre à '1' pour les GPIOs 13, 19, 41, 45 ou 53
        couleur (Color): Objet prenant 3 paramètres correspondant à l'intensité \ 
            de chaque LED. Les 3 trois valeurs doivent être comprises entre 0 et 255
        wait_ms (int, optional): Temps d'attente entre chaque allumage de LED. Par défaut à 50.
    """
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, couleur)
        strip.show()
        sleep(10/500.0)

def affichageTrame(strip, donnees):
    """Permet d'allumer le ruban LED en fonction des données extraite d'une trame \
        reçue et passée en paramètre

    Args:
        strip (Adafruit_NeoPixel): Objet instancié représentant le ruban LED \
            prennant 7 paramètres : \
                LED_COUNT : Nombre total de LED disponible sur le ruban\
                LED_PIN : Correspond au numéro du PIN utilisé pour transmettre les \
                    données (GPIO 18)
                LED_FREQ_HZ : Fréquence des LED en Hertz
                LED_DMA : Canal utilisé pour générer le signal
                LED_BRIGHTNESS : Intensité des LED (0 = sombre, 255 = lumineux)
                LED_INVERT : Inverse le signal (True pour inversé)
                LED_CHANNEL : Mettre à '1' pour les GPIOs 13, 19, 41, 45 ou 53
        donnees (tab[integer]): Correspond aux données contenu dans la trame
    """
    i = 0
    led = 0
    while(i < (len(donnees))):
        couleur = Color(donnees[i], donnees[i + 1], donnees[i + 2])
        colorisation(strip, couleur, led)
        sleep(10/500.0)
        i = i + 3
        led = led + 1

def etudeTrame(messageClient):
    """Fonction permettant d'extraire les données d'une trame passé en paramètre \
        et vérifie si la trame est divisible par 3 et que la longueur des données est \
        cohérente avec ce qui à été annoncé dans la trame

    Args:
        messageClient (tab[0x]): Trame envoyée par le client

    Returns:
        tab[int]: Tableau contenant les données si celui-ci est conforme. \
            Retourne 0 s'il ne l'est pas 
    """
    tailleDonnees = messageClient[45:47]
    tailleDonnees = tailleDonnees[0]*100 + tailleDonnees[1]
    donnees = messageClient[47:]

    if((tailleDonnees % 3 == 0) and (len(donnees) == tailleDonnees)):
        return donnees
    else:
        return 0

def demarrageServeur(strip):
    """Fonction qui lance le serveur UDP, récupère les trames, extrait les données \
        pour vérifier leur conformité et agit en fonction du résultat : \
            - si elles sont conforme alors le ruban LED s'allume, \
            - si elles ne le sont pas, alors le ruban LED ne s'allume pas et un message \
                d'erreur apparaît

    Args:
        strip (Adafruit_NeoPixel): Objet instancié représentant le ruban LED \
            prennant 7 paramètres : \
                LED_COUNT : Nombre total de LED disponible sur le ruban\
                LED_PIN : Correspond au numéro du PIN utilisé pour transmettre les \
                    données (GPIO 18)
                LED_FREQ_HZ : Fréquence des LED en Hertz
                LED_DMA : Canal utilisé pour générer le signal
                LED_BRIGHTNESS : Intensité des LED (0 = sombre, 255 = lumineux)
                LED_INVERT : Inverse le signal (True pour inversé)
                LED_CHANNEL : Mettre à '1' pour les GPIOs 13, 19, 41, 45 ou 53
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    s.bind(("0.0.0.0", 1024))
    print("Serveur UDP à l'écoute")

    while(True):

        reception = s.recvfrom(1024)
        messageClient = reception[0]
        addresseClient = reception[1]
        donnees = etudeTrame(messageClient)

        if(donnees):
            affichageTrame(strip, donnees)
            reponseServeur = str.encode("Réception Trame OK")

        else:
            reponseServeur = str.encode("Trame non conforme")

        s.sendto(reponseServeur, addresseClient)