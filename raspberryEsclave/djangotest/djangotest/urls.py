"""djangotest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import settings
from . import views

urlpatterns = [
    # Page d'accueil permettant de vérifier que le site est OK
    path('', views.home, name='home'),
    
    # Page "historique" utilisée pour lancer l'animation "noel" et vérifier que les rubans led fonctionnent
    path('testjoseph5', views.testjoseph5, name='testjoseph5'),
    
    # URL appellée par la Raspberry maitre pour lancer une animation dont le nom du script (par exemple "noel.py") est fourni en POST
    path('ledAllumer', views.ledAllumer, name='ledAllumer'),
    
    # LG 20241129 déac : URL sans intérêt path('ledAllumerLocal', views.ledAllumerLocal, name='ledAllumerLocal'),
    
    # URL à utiliser pour tester un fonctionnement identique à ledAllumer, mais pour laquelle le nom du script (par exemple "noel.py") est fourni en GET
    path('ledAllumerGet', views.ledAllumerGet, name='ledAllumerGet'),
    
    # URL permettant d'éteindre le ruban led (appellée par Raspberry maitre, mais peut aussi être appelée manuellement)'
    path('stop', views.stop, name='stop'),

    # Toutes les URLs ci-dessous sont obsolètes mais peuvent contenir des idées intéressantes
    path('polarLedProject', views.polarLedProject, name='polarLedProject'),
    path('testjoseph', views.testjoseph, name='testjoseph'),
    path('testjoseph2', views.testjoseph2, name='testjoseph2'),
    path('testjoseph3', views.testjoseph3, name='testjoseph3'),
    path('testjoseph4', views.testjoseph4, name='testjoseph4'),
    path('testflolulu', views.testflolulu, name='testflolulu'),
    path('testjoseph6', views.testjoseph6, name='testjoseph6'),
    path('create', views.create, name='create'),
    path('error', views.error, name='error'),
    path('api/raspberry/shutdown', views.Eteindre.as_view(), name='shute'),
    path('shutdown', views.shutdown, name='shutdown'),
    path('reboot', views.reboot, name='reboot'),
    path('api/trame/test',  views.Test.as_view(), name='Test'),
    path('api/configuration', views.IpUnivers.as_view(), name='Universe'),
    path('testLG', views.testLG, name='testLG')
]