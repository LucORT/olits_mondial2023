from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
import os
from json import dump, load
import json
from django.views.decorators.http import require_http_methods
# import requests
from ipaddress import IPv4Address
import djangotest.Fichier_commun.functions as functions
import djangotest.Fichier_commun.constante as const
import time
import os.path
import subprocess
import threading        # LG 20241129

import os.path
from sys import path
from pathlib import Path
BASE_DIR = str(Path(__file__).resolve().parent.parent.parent)
BASE_DIR2 = str(Path(__file__).resolve().parent.parent.parent)
fichierFinBoucle = str(BASE_DIR2) + '/Script/finBoucle.sh'
# print(fichierFinBoucle)
# exit(0)

# Définition d'une fonction home
def home(request):
    # Prend le fichier dans template
    template = loader.get_template('home.html')
    # Appelé body.html
    return HttpResponse(template.render())
    # Renvoi la page web en réponse

# LG 20241128 début
# def ledAllumer(request):
    
#     file = request.POST['y']
    
# # LG 20241128 old    subprocess.Popen(['sudo','python3','/home/pi/polar-led-project/djangotest/djangotest/viewsExec.py',file])
#     fichierScript = BASE_DIR + '/djangotest/djangotest/viewsExec.py'
#     print("fichierScript = " + fichierScript)
#     print("file = " + file)
#     subprocess.Popen(['sudo','python3', fichierScript, file])
    
#     return HttpResponse('ok')
    
# def ledAllumerLocal(request):
    
#     file = "blanc.py"
    
# # LG 20241128 old    subprocess.Popen(['sudo','python3','/home/pi/polar-led-project/djangotest/djangotest/viewsExec.py',file])
#     subprocess.Popen(['sudo','python3', BASE_DIR + '/djangotest/viewsExec.py', file])
    
#     return HttpResponse('ok')
def ledAllumer(request):
    if "y" in request.POST:
        file = request.POST['y']
        return ledAllumer_Commun(request, file)
    else:
        return HttpResponse("Le paramètre POST 'y' qui précise le script à lancer n'a pas été fourni : l'animation n'est pas lancée.")


# LG 20241129 déac : URL sans intérêt
# def ledAllumerLocal(request):
#     file = "blanc.py"
#     return ledAllumer_Commun(request, file)

def ledAllumerGet(request):
    if "y" in request.GET:
        file = request.GET['y']
        return ledAllumer_Commun(request, file)
    else:
        return HttpResponse("Le paramètre GET 'y' qui précise le script à lancer n'a pas été fourni : l'animation n'est pas lancée.")
        
# Fonction privée utilisée par les trois variantes de ledAllumer
# LG 20241129
def ledAllumer_Commun(request, file):
    if False:
        # Lancement direct, fonctionnement obsolète
        fichierScript = BASE_DIR + '/djangotest/djangotest/viewsExec.py'
        print("fichierScript = " + fichierScript)
        print("file = " + file)
        subprocess.Popen(['sudo','python3', fichierScript, file])
    
        return "OK"
    else:
        # Lancement dans un thread
        message = ""
        if os.path.exists(fichierFinBoucle):
            # Une animation est en cours : il faut la stopper avant de relancer la suivante, et attendre un peu
            os.remove(fichierFinBoucle)
            time.sleep(1)
            print("ledAllumer_Commun : l'animation précédente a été arrêtée")
            message += "L'animation précédente a été arrêtée; "

        filePath = BASE_DIR + "/Script/" + file
        if bool(os.path.exists(filePath)):
            message += f"Le script {file} a été lancé dans un nouveau thread"
            print(f"ledAllumer_Commun : le script {file} a été lancé dans un nouveau thread")
            threading.Thread(target=ledAllumer_Action, args=[filePath]).start()
        else:
            message += f"Le script {file} n'a pas été trouvé (cherché dans {filePath})"

    return HttpResponse(message)

# Fonction privée utilisée pour le lancement d'une animation dans un thread
def ledAllumer_Action(filePath):
    if bool(os.path.exists(filePath)):
        commande = f"sudo python3 \"{filePath}\""
        os.system(commande)
    else:
        pass

# LG 20241128 fin

# Fonction d'extenction des rubans led
# Le principe est de supprimer le fichier que les boucles d'allumage des leds checkent à chaque itération (pour sortir de la boucle quand le fichier n'existe plua)
def stop(request):
#    file_path = rC:\Users\b.mazard\Desktop\Ressources\Projet Lumières\olits_mondial2023\raspberryEsclave\djangotest\finBoucle.sh"
# LG 20241128 old    file_path = 'finBoucle.sh'
    if os.path.exists(fichierFinBoucle):
        os.remove(fichierFinBoucle)
        message = "fichier supprimé avec succès : arrêt demandé"
    else:
        message = "le fichier n'existait pas : l'arrêt avait été demandé"
    
    # Renvoi la page web en réponse
    return HttpResponse(message)



# -----------------------------------------------------------------------------------
# Toutes les fonctions ci-dessous sont obsolètes mais peuvent contenir des idées intéressantes
# -----------------------------------------------------------------------------------

def polarLedProject(request):
    # Prend le fichier dans template
    template = loader.get_template('polarLedProject.html')
    # Appelé body.html
    return HttpResponse(template.render())
    # Renvoi la page web en réponse

def testjoseph(request):
    # Prend le fichier dans template
    template = loader.get_template('testjoseph.html')
    # Appelé testjoseph.html
    return HttpResponse(template.render())
    # Renvoi la page we.b en réponse
    
def error(request):
    # Prend le fichier dans template
    template = loader.get_template('error.html')
    # Appelé testjoseph.html
    return HttpResponse(template.render())
    # Renvoi la page we.b en réponse

def testjoseph2(request):
    context = {
        'maVariable': 'toto',
        'y': request.POST['y'],
    }
    # Prend le fichier dans template
    template = loader.get_template('testjoseph2.php')
    # Appelé testjoseph2.html
    return HttpResponse(template.render(context,request))
    # Renvoi la page web en réponse
    
def testjoseph3(request):
    context = {
        'maVariable': 'toto',
        'y': request.POST['y'],
    }
    if request.POST['y']:
        # Prend le fichier dans template
        template = loader.get_template('testjoseph2.php')
    else :
        # Prend le fichier dans template
        template = loader.get_template('error.html')
    
    # Appelé testjoseph2.html
    return HttpResponse(template.render(context,request))
    # Renvoi la page web en réponse
    
def testjoseph4(request):
    context = {
        'file': request.POST['y'],
    }
    file = request.POST['y']
    path = '/home/pi/polar-led-project/Ruban_LED/Archives/' + file
    
    if bool(os.path.exists(path)):
        # Prend le fichier dans template
        f = open("fileExecute.sh", "w")
        f.write("sudo python3 " + path)
        f.close()
        
        exec = 'sh fileExecute.sh'
        #commande qui exécute le script
        os.system(exec)
        
        template = loader.get_template('testjoseph2.php')
        
    else :
        # Prend le fichier dans template
        template = loader.get_template('error.html')
    # Appelé testjoseph2.html
    return HttpResponse(template.render(context,request))
    # Renvoi la page web en réponse
    
def testjoseph5(request):   
    file = "/Script/noel.py"
    
# LG 20241128 old    q = open("finBoucle.sh", "w")
    print(fichierFinBoucle)
    q = open(fichierFinBoucle, "w")
    q.write("sudo python3")
    q.close()
    
    #path = '/home/pi/polar-led-project/Script/' + file
    
    if bool(os.path.exists(BASE_DIR + file)):
        # Prend le fichier dans template
        f = open("fileExecute.sh", "w")
        f.write("sudo python3 " + BASE_DIR + file)
        f.close()
        
        exec = 'sh fileExecute.sh'
        #commande qui exécute le script
        os.system(exec)
        
        # template = loader.get_template('testjoseph5.html')
    else :
        # Prend le fichier dans template
        # template = loader.get_template('error.html')
        pass
        
    # Appelé testjoseph2.html
#    return HttpResponse(template.render(request))
    return HttpResponse(f"Le script est lancé : {BASE_DIR + file}")
    # Renvoi la page web en réponse

def testflolulu(request):
    #Chemin de la boucle finis
    boucleFinie = '/home/pi/polar-led-project/djangotest/boucleFinie.sh'
    #Chemin de la bouclefile 
    finBoucle = '/home/pi/polar-led-project/djangotest/finBoucle.sh'
    # si existe boucleFinie
    if os.path.exists(boucleFinie):
        # si existe boucleFinie supprimer
        os.remove(boucleFinie)
    #si finBoucle.sh existe
        
    if os.path.exists(finBoucle):
        #supprimer finBoucle.sh
        os.remove(finBoucle)
        #Tant que bouclefinie n'existe pas 
        while not os.path.exists(boucleFinie):
            #il attend 10 milliseconde
            time.sleep(0.01)
            
            #Appel noel.py dans la variable 
    file = "noel.py"
    #création du fichier finBoucle.sh
    q = open("finBoucle.sh", "w")
    #fermeture du fichier
    q.close()
    
    #chemin d'accès au fichier
    path = '/home/pi/polar-led-project/Script/' + file
    #tant que le fichie
    while True:
        if bool(os.path.exists(path)):
            # Prend le fichier dans template
            f = open("fileExecute.sh", "w")
            f.write("sudo python3 " + path)
            f.close()
        
            exec = 'sh fileExecute.sh'
            #commande qui exécute le script
            os.system(exec)
            if os.path.exists(finBoucle):
                break
        
            template = loader.get_template('testjoseph5.html')
        else :
        # Prend le fichier dans template
            template = loader.get_template('error.html')

        p = open("boucleFinie.sh", "w")        
        p.close()
    
    # Appelé testjoseph2.html
        return HttpResponse(template.render(request))
    # Renvoi la page web en réponse
    
def shutdown (request):
        cmd1 = "sudo shutdown -h now" 
        #commande pour éteindre le Raspberry
        os.system(cmd1)
        #dit au systeme du Raspberry d'exécuté la commande
        html = """
        Raspberry éteinte
        """
        return HttpResponse(html) #réponse http perçu

def reboot (request):
        cmd2= "sudo reboot" 
        #commande pour reboot le Raspberry
        os.system(cmd2)
        #dit au systeme du Raspberry d'exécuté la commande
        html = """
        Raspberry reboot
        """
        return HttpResponse(html) #réponse http perçu
    
def testjoseph6(request):
    context = {
        'file': request.POST['y'],
    }
    file = request.POST['y']
    
    q = open("finBoucle.sh", "w")
    q.write("sudo python3")
    q.close()
    
    path = '/home/pi/polar-led-project/Script/' + file
    
    if bool(os.path.exists(path)):
        # Prend le fichier dans template
        f = open("fileExecute.sh", "w")
        f.write("sudo python3 " + path)
        f.close()
        
        exec = 'sh fileExecute.sh'
        #commande qui exécute le script
        os.system(exec)
        
        template = loader.get_template('testjoseph5.html')
    else :
        # Prend le fichier dans template
        template = loader.get_template('error.html')
    
    # Appelé testjoseph2.html
    return HttpResponse(template.render(request))
    # Renvoi la page web en réponse
    
def create(request):
    f = open("finBoucle.sh", "w")
    f.write("sudo python3")
    f.close()
    
    template = loader.get_template('error.html')
    
    # Appelé testjoseph2.html
    return HttpResponse(template.render())
    # Renvoi la page web en réponse


class Eteindre(View): #ressource de l'api pour éteindre le raspberry
    def post(self, request):
        cmd1 = "sudo shutdown -h now" 
        #commande pour éteindre le Raspberry
        os.system(cmd1)
        #dit au systeme du Raspberry d'exécuté la commande
        html = """
        Raspberry éteinte
        """
        return HttpResponse(html) #réponse http perçu
    

class Test(View): #ressource du raspberry pour tester le ruban led
    def post(self, request):
        cmd6 = "sh test.sh"
        #commande qui exécute le script test.sh
        os.system(cmd6)
        #exécute directement sur le système du Raspberry la commande
        html = """
        Eclairage LED
        """
        return HttpResponse(html) #réponse http perçu

class IpUnivers(View): #ressource du raspberry pour la configurationd e l'univers et de l'ip
    def post(self, request):
        data = json.loads(request.body)#parse, décode le json envoyer dans la requete par la fonction fetch() en javacscript
        with open(const.DATA, mode='w+') as file: #ouvr ele document en mode éditeur et écrase les données d'avant
            file.write(str(request.body, 'UTF-8')) #écrit le contenu de la requete envoyer dans le fichier json
            
        with open(const.DATA, 'r') as file: #ouvre le document data.jon en mdoe lecture
            data = load(file) #charge le document dans la variable data
            ip = data["ip"] #définition de la variable ip en isolant le contenu json
            univers = data["univers"] #définition de la variable univers en isolant le contenu json
            # check(ip)
            try:
                IPv4Address(ip)#regarde si c'est bien une adresse de type ipv4
            except: #sinon bloque l'envoie et met une erreur 400 en reponse html
                html="""
                400
                """
                return HttpResponse(html)
            try:
                y = json.loads(univers) #parse, décode le json univers
            except: #si ca marche pas alors il y a une erreur 400 retourner
                html="""
                400
                """
                return HttpResponse(html)
            time.sleep(10) #attend 10 secondes avant d'exécuté la prochaine fonction
            functions.valideIpIhm(ip) #éxécute la fonction valideIpIhm qui change l'ip du Raspberry et redémarre celui-ci
        #variable
        html="""
        Données envoyées dans data.json
        """
        return HttpResponse(html) #si tout ce passe bien c'est la réponse html renvoyé

def testLG(request):
    html = "Test LG, sur " + os.path.abspath(__file__)
    return HttpResponse(html)
