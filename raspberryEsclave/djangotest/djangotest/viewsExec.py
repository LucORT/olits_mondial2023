# Lancer une animation via un script créé à la volée
# Exemple de commande de lancement(depuis le répertoire de base du projet) : python3.10 "raspberryEsclave/djangotest/djangotest/viewsExec.py" "blanc.py"
# TODO : ce fichier est obsolète : à supprimer (vérifier avant...)

# from django.shortcuts import render
# from django.http import HttpResponse
# from django.template import loader
# from django.views.decorators.csrf import csrf_exempt
# from django.views.generic import View
# from json import dump, load
# import json
# from django.views.decorators.http import require_http_methods
# import requests
# from ipaddress import IPv4Address
# import time

# LG 20241129 déac import os
import os.path
import sys
from pathlib import Path #eddy

# LG 20241129 début
if len(sys.argv) < 2:
    print("Ce programme ne fonctionne que si on lui fournit en paramètre le nom du script à exécuter (ex : \"blanc.py\")")
    exit(1)
# LG 20241129 fin

chemin_complet = os.path.realpath(__file__)
cheminLocal = os.path.dirname(chemin_complet)
cheminScripts = os.path.dirname(Path(chemin_complet).resolve().parent.parent) + "/Script"

# print("cheminLocal = " + cheminLocal)
# print("cheminScripts = " + cheminScripts)

# LG 20241129 déac début : cette création est maintenant faite dans chaque script de pilotage des leds (ex : "blanc.py")
# # LG 20241128 old q = open("/home/pi/polar-led-project/djangotest/finBoucle.sh", "w")
# fichierFinBoucle = cheminLocal + "/finBoucle.sh"
# q = open(fichierFinBoucle, "w")
# # LG déac q.write("sudo python3")
# q.close()
# LG 20241129 déac fin

file = sys.argv[1]
# LG 20241128 old path = '/home/pi/polar-led-project/Script/' + file
path = cheminScripts + "/" + file
print("path = " + path)

if bool(os.path.exists(path)):
    # Prend le fichier dans template
    f = open("fileExecute.sh", "w")
# LG 20241129 old    f.write("sudo python3 " + path)
    f.write('sudo python3 "' + path + '"')
    f.close()
    
    exec = 'sh fileExecute.sh'
    #commande qui exécute le script
    os.system(exec)
else:
    print("path non trouvé (" + path + ")")
    
    



