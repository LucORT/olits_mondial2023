try {

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function valideripuniv() {
        var formatip = /((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}/; //regex pour le format ipv4
        var formatuniv = /(?:^|[^0-9-])(?:-[1-9]|-3276[0-8]|-?[1-9][0-9]{1,3}|-?[12][0-9]{4}|-?3[01][0-9]{3}|-?32[0-6][0-9]{2}|-?327[0-5][0-9]|[0-9]|3276[0-7])/; //regex compris entre 1 et 32 678
        const valip = document.querySelector('#ip');
        const valuniv = document.querySelector('#univers');
        valip.addEventListener('keyup', () => { //écoute à chaque ffois qu'une touche ce leve
            valip.setCustomValidity('')
            if (!formatip.test(valip.value)) { //si dans le champs ip on a pas des choses compris dans le regex
                valip.setCustomValidity('rentrer une ip ou finir de remplir le champs'); //ca marque cette erreur
                console.log('invalide'); //marque invalide en console
            }
            valip.checkValidity();//refresh pour voir q'il peut enlever l'erreur
            valip.reportValidity(); //reporte la valeur (erreur ou non)
        });
        valuniv.addEventListener('keyup', () => { //même scénario mais avec le champ univers et le regex univers
            valuniv.setCustomValidity('')
            if (!formatuniv.test(valuniv.value)) {
                valuniv.setCustomValidity('rentrer un nombre compris entre 1 et 32768');
                console.log('invalide');
            }
            valuniv.checkValidity();
            valuniv.reportValidity();
        });
    }
    valideripuniv(); //exécute la foction tout le temps qu'il y a une touche pressé dans le formulaire


    function Verification() {
        //Récupérer la valeur des champs IP et univers
        var ip = document.getElementById('ip').value;
        var univers = document.getElementById('univers').value;

        //controle sur l'ip
        if (ip == '') {
            alert('Vous devez completer le champs ip !');
            document.getElementById('ip').style.backgroundColor = "red";
            document.getElementById('ip').style.color = "#FFF";

            //permet de bloquer l'envoi du formulaire
            return false;
        }
        else {
            document.getElementById('ip').style.backgroundColor = "#9C6";
        }

        //controle de l'univers
        if (univers == '') {
            alert('Vous devez compléter le champs univers !');
            document.getElementById('univers').style.backgroundColor = "red";
            document.getElementById('univers').style.color = "#FFF";

            //permet de bloquer l'envoi du formulaire
            return false;
        }
        else {
            document.getElementById('univers').style.backgroundColor = "#9C6";
        }
    }

    async function Trame() {
        //confirme l'envoi de la trame sur le ruban LED
        if (confirm("Envoi de la trame test?")) {
            txt = "Trame test envoyée";
            let csrftoken = getCookie('csrftoken');
            let headers = new Headers();
            headers.append('X-CSRFToken', csrftoken);
// /home/pi/polar-led-project/djangotest/test.sh
            fetch('api/trame/test', { //api pour allumer et tester les LED si elle fonctionne
                method: 'POST',
                headers,
                credentials: "include"
            })
        }
        let form = document.querySelector('#ipuniv')
        let button = document.querySelector('buttonsend')
        let response = await fetch('api/configuration', {
            //API qui devrait envoyer les données formulaires
            method: 'POST',
            header: 'application.json',
            body: JSON.stringify({
                ip: document.getElementById('ip').value,
                univers: document.getElementById('univers').value,
            })
        })
        console.log(response);

        if (!reponse.ok) {
            throw new Error("Erreur avec la requête")

        }
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms)); //fonction qui 
        //retourne une promesse sleep avec en paramètre le temps en ms
    }

    async function ping(ip, port) {
        // let serverip = "http://" + document.getElementById('ip').value;
        let serverip = `http://${document.getElementById('ip').value}`; //permet de prendre l'ip rentré dans le champ
        let pong = false; // initialisation de la réponse en faux

        while (!pong) { //boucle tant que la réponse contraire
            try {
                const response = await fetch(serverip); //faire une recherche d'url sur serverip
                if (response.ok) { //si la reponse est ok
                    pong = true; //afficher la réponse
                } else {
                    console.log('Mauvaise réponse du réseau'); //sinon mettre une erreur en log
                }
                await sleep(5000); //attendre 5secondes et recommencer
            } catch (e) {
                console.error(e);
            }
            location.href = serverip; //return l'url
        }
    }

    async function envoyer() {
        ping();
        await Trame();
        Verification();
    }

    function Shutdown() {
        if (confirm("Voulez-vous vraiment éteindre le Rapsberry ?")) {
            //message boxe cancel ou confirme le clic sur le bouton
            let csrftoken = getCookie('csrftoken')
            //utilise le cookie pour créé un token
            let headers = new Headers();
            //fais une en-tête au message
            headers.append('X-CSRFToken', csrftoken);
            fetch('api/raspberry/shutdown', {
                //méthode fetch qui récupère les ressources
                //permet d'envoyer une requete via une requete post
                //utilise url de l'api shutdown pour exécuter une commande
                method: 'POST',
                headers,
                credentials: "include"
            })
        }
    }

    document.querySelector('[name=buttonsend]')
        .addEventListener('click', envoyer); //écoute le clique sur le bouton est exécute la fonction envoyer au clique
	

  

} catch (exception) {
    console.error(exception);
}
