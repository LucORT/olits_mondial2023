# Initialisation d'une raspberry pour devenir une raspberry esclave : installation de toutes les bibliothèques nécessaires, et desu services de lancement
# Script PolarLed, à modifier pour Mondial

# Recuperation des mises a jour
sudo apt update -y
sudo apt upgrade -y

# Instalation de python3
sudo apt install -y python3
# Installation des packages necessaire au fonctionnement de l'ecran
sudo apt-get install -y i2c-tools libi2c-dev python-smbus gcc make build-essential python-dev scons swig
sudo apt install -y python3-dev python-pil python3-pil python3-pip python3-setuptools python3-rpi.gpio python3-gpiozero
# Installation de django
pip3 install django 

# Configuration de I2C
sudo cp polar-led-project/file_init/config.txt /boot
sudo cp polar-led-project/file_init/modules /etc
sudo cp polar-led-project/file_init/99-i2c.rules /etc/udev/rules.d

# Clone et installation de la librairie utilisée par l'ecran
cd
git clone https://github.com/adafruit/Adafruit_Python_SSD1306.git
cd Adafruit_Python_SSD1306
sudo python3 setup.py install
# Clone et installation de la librairie du ruban led
cd
git clone https://github.com/jgarff/rpi_ws281x.git
cd rpi_ws281x
sudo scons
cd python
sudo python3 setup.py build
sudo python3 setup.py install
sudo pip3 install adafruit-circuitpython-neopixel

# Copie des services dans le dossier contenant tous les services (/etc/systemd/system)
sudo cp /home/pi/polar-led-project/service/* /etc/systemd/system

# Activation des services
sudo systemctl enable screen.service
sudo systemctl enable server-ihm.service
sudo systemctl enable server-udp.service

# Demarrage des services
sudo systemctl start screen.service
sudo systemctl start server-ihm.service
sudo systemctl start server-udp.service

sudo reboot
