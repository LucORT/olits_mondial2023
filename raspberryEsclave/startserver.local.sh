# Lancer le serveur Django des raspberry esclave du projet MondialDesMetiers
# Ce fichier est le fichier local, non géré par Git, destiné à une machine de test
# Commande (depuis le répertoire de base du projet) : "bash raspberryEsclave/startserver.local.sh"
repertoireSources="/home/luc/partage_win10/Luc/Lycée ORT/C2SI/2024-2025 3OLEN/Initiaux/Période 2, Mondial 2024/projetmondialdesmetiers_2023_LucORT"
source "$repertoireSources/myenv/bin/activate"
python3 "$repertoireSources/raspberryEsclave/djangotest/manage.py" runserver 0.0.0.0:8083

# sur poste de LG
# cd "/home/luc/partage_win10/Luc/Lycée ORT/C2SI/2023-2024 3OLEN/Initiaux/Mondial2023/projetmondialdesmetiers_2023_LucORT/raspberryEsclave/djangotest"
# python3 manage.py runserver 0.0.0.0:8088
# ou /usr/bin/python3 "/home/luc/partage_win10/Luc/Lycée ORT/C2SI/2023-2024 3OLEN/Initiaux/Mondial2023/projetmondialdesmetiers_2023_LucORT/raspberryEsclave/djangotest/manage.py" runserver 0.0.0.0:8088
