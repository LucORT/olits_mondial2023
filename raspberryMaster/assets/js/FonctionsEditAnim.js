var num = $(".etape").length;
function AddStep(){
    num += 1;
    $('#Etapes').append('<fieldset id="Etape'+num+'" style="margin: 1px;width: 32%;">' +
        '<legend>Etape '+num+'</legend><label class="form-label" for="IntensiteHameg'+num+'">Intensité</label>' +
        '<input class="form-range form-control" id="IntensiteHameg'+num+'" type="range" name="Etape'+num+'Intensite" style="margin: 5px;margin-top: 0;width: 95%;">' +
        '<label class="form-label" for="TempsHameg'+num+'">Durée</label>' +
        '<input class="form-control form-control-sm" id="TempsHameg'+num+'" type="number" name="Etape'+num+'Temps" placeholder="Temps pour l\'étape" style="margin: 5px;margin-top: 0;width: 95%;">' +
        '</fieldset>')
    $('#NbStep').val(num);
    console.log(num);
};

function RemoveLastStep(){
    var stepID = '#Etape'+num;
    if(num > 1){
        num -= 1;
        $(stepID).remove();
        $('#NbStep').val(num);
    }
};