<?php
$path = __DIR__ . "\config.json";

// si on clique sur lancer
if (isset($_POST['Lancer'])) {

    // fonction qui appelle le script
    function file_post_contents($url, $data) {
        $postdata = $data;

        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => "Content-type: application/json",
                'content' => $postdata
            )
        );

        $context = stream_context_create($opts);
        return file_get_contents($url, false, $context);
    }

    if ($_POST["scriptRubanLed1"] && $_POST["urlRubanLed1"]) {
        $tab = array("y" => $_POST["scriptRubanLed1"]);
        $tab['couleur'] = json_decode($_POST["colpick1"]);
        $data = json_encode($tab);
        $url = $_POST["urlRubanLed1"] . "testjoseph7";
        echo file_post_contents($url, $data);
        var_dump($data);
    }

    if ($_POST["scriptRubanLed2"] && $_POST["urlRubanLed2"]) {
        $tab = array("y" => $_POST["scriptRubanLed2"]);
        $tab['couleur'] = json_decode($_POST["colpick2"]);
        $data = json_encode($tab);
        $url = $_POST["urlRubanLed2"] . "testjoseph6";
        echo file_post_contents($url, $data);
        var_dump($data);
    }

    if ($_POST["scriptRubanLed3"] && $_POST["urlRubanLed3"]) {
        $tab = array("y" => $_POST["scriptRubanLed3"]);
        $tab['couleur'] = json_decode($_POST["colpick3"]);
        $data = json_encode($tab);
        $url = $_POST["urlRubanLed3"] . "testjoseph6";
        echo file_post_contents($url, $data);
        var_dump($data);
    }

    if ($_POST["scriptRubanLed4"] && $_POST["urlRubanLed4"]) {
        $tab = array("y" => $_POST["scriptRubanLed4"]);
        $tab['couleur'] = json_decode($_POST["colpick4"]);
        $data = json_encode($tab);
        $url = $_POST["urlRubanLed4"] . "testjoseph6";
        echo file_post_contents($url, $data);
        var_dump($data);
    }

    function file_post_contents_lampe($url, $data) {
        $postdata = $data;

        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => "Content-type: application/json",
                'content' => $postdata
            )
        );

        $context = stream_context_create($opts);
        return file_get_contents($url, false, $context);
    }

    $jsonAnim = file_get_contents("animation.json");
    $tabAnim = json_decode($jsonAnim, true);
    $jsonURL = file_get_contents("url.json");
    $tabURL = json_decode($jsonURL, true);
    $hameg = $tabURL['HAMEG'];
    $portcom = $hameg['port-com'];
    $tabEnvoi = [];

    if ($_POST["Lampe1"] && $_POST["urlLampe1"]) {
        foreach ($tabAnim as $anim) {
            if ($anim['nom'] == $_POST['animationLampe1']) {
                $tab = array('channel' => $_POST['channelLampe1']);
                $tab['port-com'] = $portcom;
                $tab['liste'] = $anim['liste'];
                array_push($tabEnvoi, $tab);
            }
        }
    }

    if ($_POST["Lampe2"] && $_POST["urlLampe2"]) {
        foreach ($tabAnim as $anim) {
            if ($anim['nom'] == $_POST['animationLampe2']) {
                $tab = array('channel' => $_POST['channelLampe2']);
                $tab['port-com'] = $portcom;
                $tab['liste'] = $anim['liste'];
                array_push($tabEnvoi, $tab);
            }
        }
    }

    if ($_POST["Lampe3"] && $_POST["urlLampe3"]) {
        foreach ($tabAnim as $anim) {
            if ($anim['nom'] == $_POST['animationLampe3']) {
                $tab = array('channel' => $_POST['channelLampe3']);
                $tab['port-com'] = $portcom;
                $tab['liste'] = $anim['liste'];
                array_push($tabEnvoi, $tab);
            }
        }
    }

    if ($_POST["Lampe4"] && $_POST["urlLampe4"]) {
        foreach ($tabAnim as $anim) {
            if ($anim['nom'] == $_POST['animationLampe4']) {
                $tab = array('channel' => $_POST['channelLampe4']);
                $tab['port-com'] = $portcom;
                $tab['liste'] = $anim['liste'];
                array_push($tabEnvoi, $tab);
            }
        }
    }

    $tabEnvoiJson = json_encode($tabEnvoi);
    $data = $tabEnvoiJson;
    var_dump($data);
    $url = $_POST["urlLampe1"] . "polls";
    echo file_post_contents_lampe($url, $data);

} elseif (isset($_POST['Eteindre'])) {

    // fonction qui exécute l'URL
    function execUrl($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url . "stop");
        curl_exec($curl);
        curl_close($curl);
    }

    if ($_POST['urlRubanLed1']) {
        $url = $_POST['urlRubanLed1'];
        execUrl($url);
    }

    if ($_POST['urlRubanLed2']) {
        $url = $_POST['urlRubanLed2'];
        execUrl($url);
    }

    if ($_POST['urlRubanLed3']) {
        $url = $_POST['urlRubanLed3'];
        execUrl($url);
    }

    if ($_POST['urlRubanLed4']) {
        $url = $_POST['urlRubanLed4'];
        execUrl($url);
    }

} elseif (isset($_POST['Enregistrer'])) {

    // on vérifie que les infos sont transmises
    if (
        isset($_POST["scriptRubanLed1"]) && isset($_POST["urlRubanLed1"]) &&
        isset($_POST["scriptRubanLed2"]) && isset($_POST["urlRubanLed2"]) &&
        isset($_POST["scriptRubanLed3"]) && isset($_POST["urlRubanLed3"]) &&
        isset($_POST["scriptRubanLed4"]) && isset($_POST["urlRubanLed4"]) &&
        isset($_POST["animationLampe1"]) && isset($_POST["urlLampe1"]) &&
        isset($_POST["animationLampe2"]) && isset($_POST["urlLampe2"]) &&
        isset($_POST["animationLampe3"]) && isset($_POST["urlLampe3"]) &&
        isset($_POST["animationLampe4"]) && isset($_POST["urlLampe4"])
    ) {
        $content = file_get_contents($path);
        $decode = json_decode($content, true);

        $sameName = false;
        $i = 0;
        $j = 0;

        while ($decode[$i]) {
            if ($decode[$i]['Nom'] == $_POST["NomEnregistrement"]) {
                $sameName = true;
                $handle = fopen($path, "r+");
                if ($handle) {
                    while (($line = fgets($handle)) !== false) {
                        if ($j == $i) {
                            $tab = array(
                                "Nom" => $_POST["NomEnregistrement"],
                                "RubanLed1" => $_POST["scriptRubanLed1"],
                                "RubanLed2" => $_POST["scriptRubanLed2"],
                                "RubanLed3" => $_POST["scriptRubanLed3"],
                                "RubanLed4" => $_POST["scriptRubanLed4"],
                                "Lampe1" => $_POST["animationLampe1"],
                                "Lampe2" => $_POST["animationLampe2"],
                                "Lampe3" => $_POST["animationLampe3"],
                                "Lampe4" => $_POST["animationLampe4"]
                            );
                            $Seria = json_encode($tab);
                            $Seria = "{$Seria}]\n";
                            file_put_contents($path, str_replace($line, $Seria, file_get_contents($path)));
                        }
                        $j++;
                    }
                }
            }
            if (!isset($decode[$i + 1])) break;
            $i++;
        }

        if (!$sameName) {
            $handle = fopen($path, "r+");
            if ($handle) {
                $tab = array(
                    "Nom" => $_POST["NomEnregistrement"],
                    "RubanLed1" => $_POST["scriptRubanLed1"],
                    "colpick1" => json_decode($_POST["colpick1"]),
                    "colpickhex1" => $_POST["colpickhex1"],
                    "RubanLed2" => $_POST["scriptRubanLed2"],
                    "colpick2" => json_decode($_POST["colpick2"]),
                    "colpickhex2" => $_POST["colpickhex2"],
                    "RubanLed3" => $_POST["scriptRubanLed3"],
                    "colpick3" => json_decode($_POST["colpick3"]),
                    "colpickhex3" => $_POST["colpickhex3"],
                    "RubanLed4" => $_POST["scriptRubanLed4"],
                    "colpick4" => json_decode($_POST["colpick4"]),
                    "colpickhex4" => $_POST["colpickhex4"],
                    "Lampe1" => $_POST["animationLampe1"],
                    "Lampe2" => $_POST["animationLampe2"],
                    "Lampe3" => $_POST["animationLampe3"],
                    "Lampe4" => $_POST["animationLampe4"]
                );
                file_put_contents($path, str_replace("]", ",", file_get_contents($path)));
                $Seria = json_encode($tab);
                $Seria = "{$Seria}]\n";
                file_put_contents($path, $Seria, FILE_APPEND);
            }
        }
    } else {
        echo 'Erreur';
    }
}

header('Location: /index.php');
exit();
