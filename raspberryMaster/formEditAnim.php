<!DOCTYPE html>
<html>
<body>
<?php
include_once 'inc/head.php';
include_once 'inc/menu.php';

$jsonAnim = file_get_contents("animation.json");
$tabAnim = json_decode($jsonAnim, true);

$jsonLampe = file_get_contents("lampe.json");
$tabLampe = json_decode($jsonLampe, true);
?>
<main class="page">
    <!-- <div>
        <form action="formEditAnim.php" method="post">
            <select name="animation">
                <?php
                    foreach($tabAnim as $anim){
                        echo '<option value="'.$anim['nom'].'">'.$anim['nom'].'</option>';
                    }
                ?>
            </select>
        </form>
    </div> -->
    <section class="clean-block features" style="padding-bottom: 0;">
        <div class="container">
            <div class="block-heading">
                <h2 id="Titre" class="text-info">Création/Modification d'une Animation</h2>
            </div>
        </div>
    </section>

    <section style="display: flex; justify-content: center;">
        <fieldset class="fieldset f-size" style="border-radius: 5px; margin: 0px; width: 50%;">
            <form class="justify-content-center" method="post" autocomplete="off" action="traitEditAnim.php">
                <div class="d-flex justify-content-center align-items-center">
                    <label class="form-label" for="NomHameg">Nom :</label>
                    <input class="form-control form-control-sm d-lg-flex" id="NomHameg" type="text" name="NomHameg"
                           placeholder="Nom de l'animation"
                           value="<?php
                                foreach($tabLampe as $lampe){
                                    $bouton = 'Modifier'.$lampe['nom'];
                                    if(isset($_POST[$bouton])){
                                        foreach($tabAnim as $anim){
                                            $animation = 'animation'.$lampe['nom'];
                                            if($anim['nom'] == $_POST[$animation]){
                                                echo $anim['nom'];
                                            }
                                        }
                                    }
                                }
                            ?>"
                           style="margin: 5px; margin-top: 0; width: 30%;">
                </div>

                <div class="d-flex justify-content-center align-items-center">
                    <label class="form-label" for="DescriptionHameg">Desc :</label>
                    <textarea class="form-control form-control-sm" id="DescriptionHameg" style="width: 30%; margin: 5px;"
                              name="DescriptionHameg" placeholder="Description de l'animation"><?php
                                foreach($tabLampe as $lampe){
                                    $bouton = 'Modifier'.$lampe['nom'];
                                    if(isset($_POST[$bouton])){
                                        foreach($tabAnim as $anim){
                                            $animation = 'animation'.$lampe['nom'];
                                            if($anim['nom'] == $_POST[$animation]){
                                                echo $anim['description'];
                                            }
                                        }
                                    }
                                }
                            ?>
                    </textarea>
                </div>

                <div style="display: none">
                    <input type="number" id="NbStep" name="NbStep">
                </div>

                <div id="Etapes" class="d-flex justify-content-center flex-wrap">
                <?php
                    $postBouton = false;
                    foreach($tabLampe as $lampe){
                        $bouton = 'Modifier'.$lampe['nom'];
                        if(isset($_POST[$bouton])){
                            $postBouton = true;
                            foreach($tabAnim as $anim){
                                $animation = 'animation'.$lampe['nom'];
                                if($anim['nom'] == $_POST[$animation]){
                                    $liste = $anim['liste'];
                                    $i = 0;
                                    foreach($liste as $etape){
                                        $i++;
                ?>
                                        <fieldset id="Etape<?php echo $i ?>" style="margin: 1px; width: 32%;" class="etape">
                                            <legend>Etape <?php echo $i ?></legend>
                                            <label class="form-label" for="IntensiteHameg">Intensité</label>
                                            <input class="form-range form-control" id="IntensiteHameg" type="range"
                                                   value="<?php echo $etape['intensite']; ?>"
                                                   name="Etape<?php echo $i ?>Intensite"
                                                   style="margin: 5px; margin-top: 0; width: 95%;">
                                            <label class="form-label" for="TempsHameg">Durée</label>
                                            <input class="form-control form-control-sm" id="TempsHameg" type="number"
                                                   value="<?php echo $etape['duree']; ?>"
                                                   name="Etape<?php echo $i ?>Temps"
                                                   placeholder="Temps pour l'étape"
                                                   style="margin: 5px; margin-top: 0; width: 95%;">
                                        </fieldset>
                <?php
                                    }
                                }
                            }
                        }
                    }
                    echo $postBouton;
                    if($postBouton == false) {
                        $i = 1;
                ?>
                    <fieldset id="Etape<?php echo $i ?>" style="margin: 1px; width: 32%;" class="etape">
                        <legend>Etape <?php echo $i ?></legend>
                        <label class="form-label" for="IntensiteHameg">Intensité</label>
                        <input class="form-range form-control" id="IntensiteHameg" type="range"
                               name="Etape<?php echo $i ?>Intensite"
                               style="margin: 5px; margin-top: 0; width: 95%;">
                        <label class="form-label" for="TempsHameg">Durée</label>
                        <input class="form-control form-control-sm" id="TempsHameg" type="number"
                               name="Etape<?php echo $i ?>Temps"
                               placeholder="Temps pour l'étape"
                               style="margin: 5px; margin-top: 0; width: 95%;">
                    </fieldset>
                <?php
                    }
                ?>
                </div>

                <button class="btn btn-primary btn-sm d-inline-flex float-end" id="button" type="button"
                        onclick="RemoveLastStep()" style="margin: 5px;">
                    Retirer Dernière Etape
                </button>
                <button class="btn btn-primary btn-sm d-inline-flex float-end" id="button" type="button"
                        onclick="AddStep()" style="margin: 5px;">
                    Ajouter étape
                </button>
                <button class="btn btn-primary btn-sm d-inline-flex float-end" id="button" type="submit"
                        style="margin: 5px;">
                    Sauvegarder
                </button>
            </form>
        </fieldset>
    </section>
</main>

<?php
include_once 'inc/footer.php';
?>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-3.6.0.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script> -->
<script src="assets/js/vanilla-zoom.js"></script>
<script src="assets/js/theme.js"></script>
<script src="assets/js/FonctionsEditAnim.js"></script>
<script>
    var animationJson = "<?php echo $jsonAnim; ?>";
</script>
</body>
</html>
