<?php
// LG 20241129 début
    if (!file_exists("url.json")) {
        echo "Le fichier url.json n'existe pas !" ;
        exit() ;
    }
    if (!file_exists("script.json")) {
        echo "Le fichier script.json n'existe pas !" ;
        exit() ;
    }
    if (!file_exists("animation.json")) {
        echo "Le fichier animation.json n'existe pas !" ;
        exit() ;
    }
    if (!file_exists("lampe.json")) {
        echo "Le fichier lampe.json n'existe pas !" ;
        exit() ;
    }
    if (!file_exists("config.json")) {
        echo "Le fichier config.json n'existe pas !" ;
        exit() ;
    }
// LG 20241129 fin

    $json = file_get_contents("url.json");
    $tabUrl = json_decode($json, true);

    $jsonScript = file_get_contents("script.json");
    $tabScript = json_decode($jsonScript, true);

    $jsonAnim = file_get_contents("animation.json");
    $tabAnim = json_decode($jsonAnim, true);

    $jsonLampe = file_get_contents("lampe.json");
    $tabLampe = json_decode($jsonLampe, true);

    $content = file_get_contents("config.json");
    $tabConfig = json_decode($content, true);

?>
<!DOCTYPE html>
<html>
<?php
include_once 'inc/head.php';
?>

<body>
    <?php
    include_once 'inc/menu.php';
    ?>
    <main class="page landing-page">
        <section class="clean-block features" style="padding-bottom: 0;">
            <div class="container">
                <div class="block-heading">
                    <h2 id="Titre" class="text-info">Choix des Animations</h2>
                </div>
            </div>
        </section>
        <section class="d-flex justify-content-center" style="margin: 15px;">
            <fieldset id="formanim" class="d-flex fieldset" style="justify-content: center;">
                <div>
                    <legend id="titre2" style="font-size: 18px; text-align: center;">Animations Ruban LED / Lampes</legend>
                    <legend id="titre2" style="font-size: 18px;">Rubans LEDs</legend>
                    <!-- <form action="index.php" method="post"> -->
                    <div class="row" style="margin-bottom: 8px;">
                        <div class="col-md-5">
                            <label>Animation générale</label>
                        </div>
                        <div class="col-md-5">
                            <select id="selectgeneral" name="selectgeneral" class="form-select form-select-sm w-auto">
                                <?php
                                $i = 0;
                                foreach ($tabScript as $value) {
                                    echo '<option value="' . $value["script"] . '" selected>' . $value['nom'] . '</option>';
                                    $i++;
                                }
                                ?>
                                <option value="" selected></option>
                            </select>
                        </div>

                        <div class="col-md-2"><input class="btn btn-primary btn-sm d-inline-flex float-end"
                                type="submit"
                                id="bgeneral"
                                name="validerConfig"
                                value="appliquer">
                        </div>
                    </div>
                    <!-- </form> -->

                    <?php
                    for ($i = 1; $i <= 4; $i++) {
                        echo '<script>
        $( "#bgeneral" ).click(function() {
            var text = $("#selectgeneral").text();
            console.log(text);
            $( "scriptRubanLed' . $i . '" ).val( text );
        });
        </script>';
                    }
                    ?>
                    <form id="animation" method="post" autocomplete="off">
                        <div style="margin-left: 61px;"> <!-- LG 20221129 -->
                            <?php
                            $n = 0;
                            if (isset($_POST['validerConfig'])) {
                                foreach ($tabConfig as $rowconfig) {
                                    if ($rowconfig['Nom'] == $_POST['selectgeneral']) {
                                        $config = $rowconfig;
                                    }
                                }
                            } else {
                                foreach ($tabConfig as $rowconfig) {
                                    $config = $rowconfig;
                                    break;
                                }
                            }
                            foreach ($tabUrl as $item) {
                                $n++;
                                $nom = $item['nom'];
                                if ($item['nom'] != "HAMEG") {
                                    echo '<div class="form-group row" style="margin-bottom: 8px;">
                        <div class="col-md-4"><label id="' . $item["nom"] . '" onclick="pyramide(),rubanLedClick()">' . $item["nom"] . '</label>&nbsp;<label></label><input type="hidden" name="' . $item["nom"] . '" value="' . $item["nom"] . '">
                        <input type="hidden" name="url' . $item["nom"] . '"value="' . $item["url"] . '"></div>
                        <div class="col-md-5"><select id="selectLed' . $item["id"] . '" class="form-select form-select-sm w-auto" name="script' . $item["nom"] . '">';
                                    foreach ($tabScript as $script) {
                                        if ($config[$nom] == $script) {
                                            echo '<option value="' . $script['script'] . '" selected>' . $script['nom'] . '</option>';
                                        } else {
                                            echo '<option value="' . $script['script'] . '">' . $script['nom'] . '</option>';
                                        }
                                    }
                                    if ($config[$nom] == "") {
                                        echo '<option value="" selected></option></select></div>';
                                    } else {
                                        echo '<option value=""></option></select></div></div>';
                                    }
                                    $jaune = '{"r":255,"g":255,"b":0}';
                                    // if(isset($config["colpick".$n])) {
                                    // echo '<div class="col-md-2"><input type="hidden" value="'.json_encode($config["colpick".$n]).'" name="colpick'.$n.'" id="colpick'.$n.'"><img id="select'.$n.'" src="assets/colorpicker/images/select.png" style="height:30px; width: auto; padding:0px; border-radius: 15%; background-color:'.$config["colpickhex".$n].' ;"></div></div>';
                                    // }else {
                                    //     echo '<div class="col-md-2"><input type="hidden" name="colpick'.$n.'" id="colpick'.$n.'"><input type="hidden" name="colpickhex'.$n.'" id="colpickhex'.$n.'"><img id="select'.$n.'" src="assets/colorpicker/images/select.png" style="height:30px; width: auto; padding:0px; border-radius: 15%; background-color:#ffff00 ;"></div></div>';
                                    // }

                                    // echo '<script>
                                    //     $(\'#select'.$n.'\').ColorPicker({
                                    //         color: \'#ffff00\',
                                    //         onShow: function (colpkr) {
                                    //             $(colpkr).fadeIn(500);
                                    //             return false;
                                    //         },
                                    //         onHide: function (colpkr) {
                                    //             $(colpkr).fadeOut(500);
                                    //             return false;
                                    //         },
                                    //         onChange: function (hsb, hex, rgb) {
                                    //             $(\'#select'.$n.'\').css(\'backgroundColor\', \'#\' + hex);
                                    //             $("#colpick'.$n.'").attr(\'value\', JSON.stringify(rgb));
                                    //             $("#colpickhex'.$n.'").attr(\'value\', \'#\' + hex);
                                    //         }
                                    //     })
                                    //     var color = "red";
                                    //     $(\'#select'.$n.'\').ColorPickerSetColor(color);
                                    // </script>';
                                } else {
                                    $urlHAMEG = $item['url'];
                                }
                            }
                            ?>
                        </div> <!-- LG 20221129 -->
                        <hr>

                        <div class="row" style="margin-bottom: 15px;">
                            <legend class="col-md-5" style="font-size: 18px;">Lampes variables</legend>
                            <input class="col-md-6"
                                id="button"
                                type="button"
                                value="Nouvelle animation"
                                onclick="window.location.href = 'formEditAnim.php';">
                        </div>

                        <div style="margin-left: 61px;"> <!-- LG 20221129 -->
                            <!-- élément enlever sur les inputs, la balise form étant privilégié -->
                            <?php
                            foreach ($tabLampe as $item) {
                                $nomLampe = $item['nom'];
                                echo '<div class="form-group row" style="margin-bottom: 8px;">
                    <div class="col-md-4"><label>' . $item["libelle"] . '</label><input type="hidden" name="' . $item["nom"] . '" value="' . $item["nom"] . '">
                        <input type="hidden" name="url' . $item["nom"] . '" value="' . $urlHAMEG . '">
                        <input type="hidden" name="channel' . $item["nom"] . '"value="' . $item["channel"] . '"></div>
                        <div class="col-md-5"><select id="select" class="form-select form-select-sm w-auto" name="animation' . $item["nom"] . '">';
                                foreach ($tabAnim as $anim) {
                                    if ($config[$nomLampe] != "" && $config[$nomLampe] == $anim['nom']) {
                                        echo '<option value="' . $anim['nom'] . '" selected>' . $anim['nom'] . '</option>';
                                    } else {
                                        echo '<option value="' . $anim['nom'] . '">' . $anim['nom'] . '</option>';
                                    }
                                }
                                if ($config[$nomLampe] == "") {
                                    echo '<option value="" selected></option></select></div>';
                                } else {
                                    echo '<option value=""></option></select></div>';
                                }
                                echo '<div class="col-md-3"><input class="btn btn-primary btn-sm d-inline-flex float-end"
                        id="button"
                        formaction="formEditAnim.php"
                        type="submit"
                        name="Modifier' . $item['nom'] . '"
                        value="Modifier"
                        style="margin-right: 5px;"';
                                //                            onclick="debugger; window.lancer = false ; window.eteindre = false ;window.location.href = ' . "'" . "formEditAnim.php" . "'" . '' :
                                echo 'onclick="debugger; window.lancer = false ; window.eteindre = false ;';
                                echo ';"></div></div>';
                            }
                            ?>
                            <!-- <div class="row"><label>Nom enregistrement</label><input type="text" class="form-control" name="NomEnregistrement"></div> -->
                            <div style="margin-top: 10px;">
                            </div> <!-- LG 20221129 -->
                            <hr>

                            <div style="display: none;"> <!-- LG 20221129 -->
                                <input class="btn btn-primary"
                                    type="submit"
                                    formaction="enregistreAnimationComplete.php"
                                    id="button"
                                    name="Enregistrer"
                                    value="Enregistrer l'animation globale"
                                    style="margin: 5px;">
                                <hr>
                            </div> <!-- LG 20221129 -->
                            <div> <!-- LG 20221129 -->
                                <input class="btn btn-primary btn-sm d-inline-flex float-end"
                                    id="cmdEteindre"
                                    type="submit"
                                    formaction="traitEnvoi.php"
                                    name="Eteindre"
                                    value="Arrêter"
                                    style="margin: 5px; width:30%;"
                                    onclick="window.lancer = false ; window.eteindre = true ;">
                                <!-- LG 20221125                 déac <form action="traitEnvoi.php"> -->
                                <input class="btn btn-primary btn-sm d-inline-flex float-end"
                                    id="cmdLancer"
                                    type="submit"
                                    name="Lancer"
                                    value="Lancer"
                                    style="margin: 5px; width:30%;"
                                    formaction="traitEnvoi.php" <?php // LG 20221125
                                                                ?>
                                    onclick="window.lancer = true ; window.eteindre = false ;">
                                <!-- LG 20221129 déac            <input type="hidden" name="Eteindre" id="Eteindre"/></br>                 -->
                            </div> <!-- LG 20221129 -->
                            <!-- LG 20221125 déac </form> -->
                        </div>
                        <div id='message'></div>
                    </form>
                </div>
            </fieldset>
            <script>
                $("#animation").submit(function(event) {
                    // return ;
                    debugger;
                    var formData = $(this).serialize();
                    if (window.lancer) {
                        //                    alert("lancer") ;
                        formData += "&Lancer=1";
                    } else if (window.eteindre) {
                        //                    alert("eteindre") ;
                        formData += "&Eteindre=1";
                    } else {
                        return;
                    }
                    event.preventDefault();
                    if (formData.includes("Lancer")) {
                        $("#message").html('Lancement en cours');
                    } else {
                        $("#message").html('Extinction en cours');
                    }
                    $.ajax({
                        url: "traitEnvoi.php",
                        type: "post",
                        data: formData
                    }).done(function(responce) {
                        if (window.lancer) {
                            $("#message").html("L'animation est lancée");
                        } else {
                            $("#message").html("La pyramide est éteinte");
                        }
                        window.lancer = false;
                        window.eteindre = false;
                    })
                });
            </script>

            <fieldset id="schema" class="d-flex fieldset" style="position: relative;">
                <div style="width: 100%;">
                    <legend id="titre2" style="font-size: 18px; text-align:center;">
                        Aperçu / schema
                    </legend>
                    <legend id="Text" style="font-size: 16px; text-align:center; height: 24px;"> </legend>

                    <div style="display: flex; justify-content: center; align-items:center;" text-align="center">
                        <img id="pyramide" src="img/pyramide_0_transparent.png" style="width: 95%; height: auto; justify-content: center; align-items:center; margin-top: 20%;">
                    </div>
                </div>
                <div style="position: absolute; bottom :0;">
                    <button class="btn btn-primary btn-sm d-inline-flex float-end" id="reset">Reset</button>
                </div>
            </fieldset>
            <fieldset id="camera" class="d-flex fieldset">
                <div style="width: 100%;">
                    <label id="titre2" class="form-label" style="width: 100%; font-size: 18px; text-align:center;">
                        Aperçu / camera
                        <!-- LG 20221129 déac                     <span id="Text"></span>                -->
                    </label>
                    
                    <!-- BGO Fix iframe -->
                        <iframe style="margin: 0;" width="100%" height="500" src="http://192.168.50.1:8000/index.html" frameborder="0" allowfullscreen scrolling="no"></iframe>
                </div>
            </fieldset>
        </section>

    </main>
    <?php
    include_once 'inc/footer.php';
    ?>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery-3.6.0.min.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>-->
    <script src="assets/js/vanilla-zoom.js"></script>
    <script src="assets/js/theme.js"></script>
    <script src="assets/js/FonctionsIndex.js"></script>
</body>

</html>
