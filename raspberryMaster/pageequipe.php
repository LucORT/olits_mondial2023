<?php
    include_once 'inc/head.php';
    include_once 'inc/menu.php';
?>

<body>
    <main class="page landing-page">
        <section class="clean-block features" style="padding-bottom: 0;">
            <div class="container">
                <div class="block-heading">
                    <h2 id="Titre" class="text-info">Equipe</h2>
                </div>
            </div>
        </section>
        <section class="clean-block features">
            <div class="container">
                <div class="block-heading" style="color: black;">
                    <h2>Equipe Dev</h2>
                    <p>IHM web: Marine et Angel</p>
                    <p>Django Led: Raphael</p>
                    <p>Scripts Django Hameg: Eddy et Benjamin</p>

                    <h2>Equipe Infra</h2>
                    <p>Rasp maitre + rubans led : Arthur et Fehisoil</p>
                    <p>Réseau: Léonie, Marwan et Matheo</p>
                    <p>Rasp esclave: Nassim et Gauthier</p>
                    <p>Sauvegardes + script install auto: Emrah </p>
                </div>
            </div>
        </section>
    </main>
<?php
    include_once 'inc/footer.php';
?>
</body>

</html>
