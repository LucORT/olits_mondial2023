<!DOCTYPE html>
<html>
<?php
    include_once 'inc/head.php';
    include_once 'inc/menu.php';
?>

<main class="page landing-page">

    <section class="clean-block features" style="padding-bottom: 0;">
        <div class="container">
            <div class="block-heading">
                <h1 id="Titre" class="text-info">Fonctionnement
                du site</h1>
            </div>
        </div>
    </section>

    <section class="d-flex justify-content-center" style="margin: 15px;">
        <fieldset class="d-flex fieldset" >
            <div>
                <h3>Fonctionnement général de l'installation </h3>
                <hr>
                <p>Une raspberry maitre pilote les raspberry esclave</p>
                <hr>
            </div>
        </fieldset>
    </section>

    <section class="d-flex justify-content-center" style="margin: 15px;">
        <fieldset class="d-flex fieldset" >
            <div>
                <legend style="font-size: 18px; text-align: center;">Animations Ruban LED / Lampes</legend>
                <p>Possibilité d'ajouter une action pour chaque ruban led</p>
                <hr>
                <legend style="font-size: 18px; text-align: center;">Lampes variables</legend>
                <p>Possibilité d'ajouter une couleur pour la pyramide </p>
            </div>
        </fieldset>
        <fieldset class="d-flex fieldset">
            <div>
                <legend style="font-size: 18px; text-align: center;">Apperçu schéma</legend>
                <p> Possibilité d'avoir un apperçu schématique de la pyramide </p>
                <hr>
                <legend style="font-size: 18px; text-align: center;">Apperçu Caméra</legend>
                <p> Possibilité d'avoir un apperçu filmé de la pyramide </p>
            </div>
        </fieldset>
    </section>
</main>

<?php
    include_once 'inc/footer.php';
?>
</body>
</html>
