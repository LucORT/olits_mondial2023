# Readme Web

> [!NOTE]
> Ce projet permet de gérer les animations de LED d’une pyramide à travers une interface web. Il est conçu pour être hébergé sur un serveur local avec un vhost pour un accès facile et personnalisé.

## Sommaire

- [Readme Web](#readme-web)
  - [Sommaire](#sommaire)
  - [Prérequis](#prérequis)
  - [Configuration](#configuration)
  - [Structure web](#structure-web)
  - [Utilisation](#utilisation)
    - [Page d'animation](#page-danimation)
    - [Page accueil](#page-accueil)
  - [Contributeur·trice·s](#contributeurtrices)

## Prérequis

- **[Apache](https://httpd.apache.org/)** : Serveur pour héberger l'application web de configuration.
- **[PHP](https://www.php.net/) probablement 7.4** :

> [!WARNING]
> Version de php à vérifier

## Configuration

1. Créez un serveur local pour héberger l'application (`vhost`).
2. Dans le répertoire du projet, copiez chaque fichier de configuration se terminant par `.modele.json` et renommez-le en supprimant l'extension `.modele`. Ces fichiers contiennent les paramètres de base à adapter selon vos besoins.

## Structure web

```plaintext
raspberryMaster/
├── assets/
├── img/								# Toutes les images utilisées
├── inc/								# Contient le footer, le head et le menu pour une inclusion facile
├── node_modules/						# Dossier des modules Node.js installés
├── tests/								# Dossier pour les fichiers de test
├── animation.json 						# Fichier de paramétrage d'animation
├── animation.modele.json 				# Fichier modèle de paramétrage d'animation
├── config.json 						# Fichier de configuration
├── config.modele.json 					# Fichier de modèle configuration
├── enregistreAnimationComplete.php 	#
├── formEditAnim.php 					#
├── index.php 							#
├── lampe.json 							# Fichier de paramétrage de lampe
├── lampe.modele.json 					# Fichier modèle de paramétrage de lampe
├── package-lock.json 					#
├── readme.md 							# Fichier README pour la documentation du projet
├── script.json 						# Fichier de script
├── script.modele.json 					# Fichier modèle de script
├── test.html 							# Fichier de test HTML
├── testTabJson.php 					#
├── toDoList.txt 						# Reste à faire
├── traitEditAnim.php 					#
├── traitEnvoi.php 						#
├── url.json 							# Fichier de configuration de ruban led avec ip
└── url.modele.json 				# Fichier modèle de configuration de ruban led avec ip
```

## Utilisation

Pour utiliser l'application, accédez à l'adresse du vhost dédiée.

### Page d'animation

Cette page permet de configurer et contrôler les animations pour les rubans LED et les lampes.

**Fonctionnalités :**

- **Animations Ruban LED / Lampes** :
  - Sélectionnez une animation spécifique pour chaque ruban LED.
  - Vous pouvez également opter pour une animation globale, qui s'appliquera à tous les rubans en même temps.

- **Lampes à couleurs variables** :
  - Choisissez une animation spécifique pour chaque couleur de lampe.

- **Contrôle global** :
  - Lancer ou arrêter toutes les animations d'un simple clic, pour une gestion centralisée.
  - Schéma de l'apperçu de la pyramide
  - Réinitialiser l'apperçu schéma de la pyramide.
  - Arrêter tout les raspberry Pi.
  - Apperçu de la pyramide en direct (caméra)

### Page accueil

Cette page présente la fonction de ce site ainsi que présente les membres de l'équipe impliqués dans le projet, avec leurs rôles et leurs contributions.

## Contributeur·trice·s

Année 2024-2025 :

- [GONNORD Marine](https://www.linkedin.com/in/marine-gonnord-7a1517234/)
- BOISSÉ Angel
