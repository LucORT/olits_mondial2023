<?php
//ajout de l'animation créer dans animation.json
$jsonAnim = file_get_contents("animation.json");
$tabAnim = json_decode($jsonAnim, true);

if( $_POST['NomHameg'] && $_POST['DescriptionHameg'] && $_POST['Etape1Intensite'] && $_POST['Etape1Temps']){
    $existe = false;
    foreach($tabAnim as $anim){
        if($anim['nom'] == $_POST['NomHameg']){
            $existe = true;
        }
    }

    if($existe){
        foreach($tabAnim as $anim){
            if($anim['nom'] == $_POST['NomHameg']){
                $k = array_search($anim, $tabAnim);
                $tab = array('nom'=> $_POST['NomHameg'], 'description' => $_POST['DescriptionHameg'], 'liste' => array(array('intensite' => intval($_POST['Etape1Intensite']), 'duree' => intval($_POST['Etape1Temps'])))) ;
                $i =2;
                while(isset($_POST['Etape'.$i.'Intensite'])){
                    $etape ='Etape'.$i;
                        array_push($tab['liste'],array('intensite' => intval($_POST[$etape.'Intensite']), 'duree' => intval($_POST[$etape.'Temps'])));
                        $i++ ;
                }
                $data = $tab;
                $replacement = array($k => $data);
                $tabAnim = array_replace($tabAnim, $replacement);
                $jsonData = json_encode($tabAnim);
                file_put_contents('animation.json', $jsonData);
            }
        }
    }else{
        $tab = array('nom'=> $_POST['NomHameg'], 'description' => $_POST['DescriptionHameg'], 'liste' => array(array('intensite' => intval($_POST['Etape1Intensite']), 'duree' => intval($_POST['Etape1Temps'])))) ;
        $i =2;
        while(isset($_POST['Etape'.$i.'Intensite'])){
            $etape ='Etape'.$i;
                array_push($tab['liste'],array('intensite' => intval($_POST[$etape.'Intensite']), 'duree' => intval($_POST[$etape.'Temps'])));
                $i++ ;
            }
        $data = $tab;
        $inp = file_get_contents('animation.json');
        $tempArray = json_decode($inp);
        array_push($tempArray, $data);
        $jsonData = json_encode($tempArray);
        file_put_contents('animation.json', $jsonData);
    }
}

header('Location: index.php');
exit();
