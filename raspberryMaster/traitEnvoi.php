<?php

// LG 20221125 déplacé de + bas début
// TODO : les fonctions file_post_contents et file_post_contents_lampe semblent identiques : il faudrait les fusionner
echo "TODO : les fonctions file_post_contents et file_post_contents_lampe semblent identiques : il faudrait les fusionner<br>" ;

function file_post_contents_lampe($url, $data) {
    $postdata = http_build_query($data);

    $opts = array('http' =>
        array(
            'method' => 'POST',
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'content' => $postdata
        )
    );

    $context = stream_context_create($opts);
    return file_get_contents($url, false, $context);
}

//fonction qui appelle le script
function file_post_contents($url, $data) {
    $postdata = http_build_query($data);

    $opts = array('http' =>
        array(
            'method' => 'POST',
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'content' => $postdata
        )
    );

    $context = stream_context_create($opts);
    return file_get_contents($url, false, $context);
}
// LG 20221125 déplacé de + bas fin

if (isset($_POST['Eteindre']) || isset($_POST['Lancer'])) {
// LG 20241129 début
//// LG 20221125 début
//    // Eteindre aussi la Hameg
//    $tabEnvoiJson = json_encode($tabEnvoi);
    $data = array('dataHameg' => $tabEnvoiJson);
    $url = $_POST["urlLampe1"]."polls/arreterAnimation";
$rep = file_post_contents_lampe($url, $data);
//    echo $rep ;
//// LG 20221125 fin
//
// LG 20221125 début
    // Eteindre aussi la Hameg
    try {
        $data = array('dataHameg' => "[]");
        $url = $_POST["urlLampe1"] . "polls/arreterAnimation" ;
        $rep = file_post_contents_lampe($url, $data);
        echo "Réponse Hameg pour la demande d'extinction : $rep<br>" ;
    } catch (\Exception $e) {
        echo "Echec de la demande d'extinction Hameg : " . $e->getMessage() . "<br>" ;
    }
// LG 20221125 fin

// LG 20241129 fin
}

//si on clique sur éteindre
//elseif(isset($_POST['Eteindre'])){
// elseif(isset($_POST['Eteindre'])){
// LG 20221130 old if(isset($_POST['Eteindre'])){
if(isset($_POST['Eteindre']) || isset($_POST['Lancer'])){
    // Si on souhaite eteindre
    // Ou si on souhaite allumer
    // Dans les deux cas, on va d'abord eteindre

    // TODO : en principe, ce traitement est obsolete car les raspberry esclave éteignent leurs leds avant de lancer une animation
    echo "OBSOLETE : en principe, ce traitement d'extinction est obsolete car les raspberry esclave éteignent leurs leds avant de lancer une animation<br>" ;

    echo "eteindre<br>" ;
    
    if($_POST['urlRubanLed1']){
        $url = $_POST['urlRubanLed1'].'stop';
// LG 20241129 début
//        file_get_contents($url);
        $rep = file_get_contents($url);
        echo "Réponse Esclave pour la demande d'extinction du ruban 1 : $rep<br>" ;
// LG 20241129 fin
    }

    if($_POST['urlRubanLed2']){
        $url = $_POST['urlRubanLed2'].'stop';
// LG 20241129 début
//        file_get_contents($url);
        $rep = file_get_contents($url);
        echo "Réponse Esclave pour la demande d'extinction du ruban 2 : $rep<br>" ;
// LG 20241129 fin
    }

    if($_POST['urlRubanLed3']){
        $url = $_POST['urlRubanLed3'].'stop';
// LG 20241129 début
//        file_get_contents($url);
        $rep = file_get_contents($url);
        echo "Réponse Esclave pour la demande d'extinction du ruban 3 : $rep<br>" ;
// LG 20241129 fin
    }

    if($_POST['urlRubanLed4']){
        $url = $_POST['urlRubanLed4'].'stop';
// LG 20241129 début
//        file_get_contents($url);
        $rep = file_get_contents($url);
        echo "Réponse Esclave pour la demande d'extinction du ruban 4 : $rep<br>" ;
// LG 20241129 fin
    }
    
    if(isset($_POST['Lancer'])){
        // On demande de lancer l'animation
        // Attendre la fin de l'extinction (pour être certain que les rubans sont bien eteints)
 	//On attends 2 secondes afin que toutes les LEDS se soit bien éteintes
        sleep(2) ;
    }
}

//si on clique sur lancer
if(isset($_POST['Lancer'])){

// LG 20221125 déplacé + haut début
//     //fonction qui appelle le script
//     function file_post_contents($url, $data) {
//         $postdata = http_build_query($data);

//         $opts = array('http' =>
//             array(
//                 'method' => 'POST',
//                 'header' => "Content-type: application/x-www-form-urlencoded\r\n",
//                 'content' => $postdata
//             )
//         );

//         $context = stream_context_create($opts);
//         return file_get_contents($url, false, $context);
//     }
// LG 20221125 déplacé + haut fin

    // TODO : dans les else de ces if, il faudrait ajouter l'instruction pour stopper l'animation ruban
    if($_POST["scriptRubanLed1"] && $_POST["urlRubanLed1"]){
        $data= array("y"=> $_POST["scriptRubanLed1"]);
        //$data["couleur1"] = $_POST["colpick1"];
        $url = $_POST["urlRubanLed1"]."ledAllumer";
// LG 20241129 début
//        echo file_post_contents($url, $data);
        $rep = file_post_contents($url, $data);
        echo "Réponse Esclave pour la demande d'animation du ruban 1 : $rep<br>" ;
// LG 20241129 fin
    }

    if($_POST["scriptRubanLed2"] && $_POST["urlRubanLed2"]){
        $data= array("y"=> $_POST["scriptRubanLed2"]);
        //$data["couleur2"] = $_POST["colpick2"];
        $url = $_POST["urlRubanLed2"]."ledAllumer";
// LG 20241129 début
//        echo file_post_contents($url, $data);
        $rep = file_post_contents($url, $data);
        echo "Réponse Esclave pour la demande d'animation du ruban 2 : $rep<br>" ;
// LG 20241129 fin
    }
    if($_POST["scriptRubanLed3"] && $_POST["urlRubanLed3"]){
        $data= array("y"=> $_POST["scriptRubanLed3"]);
        //$data["couleur3"] = $_POST["colpick3"];
        $url = $_POST["urlRubanLed3"]."ledAllumer";
// LG 20241129 début
//        echo file_post_contents($url, $data);
        $rep = file_post_contents($url, $data);
        echo "Réponse Esclave pour la demande d'animation du ruban 21 : $rep<br>" ;
// LG 20241129 fin
    }
    if($_POST["scriptRubanLed4"] && $_POST["urlRubanLed4"]){
        $data = array("y"=> $_POST["scriptRubanLed4"]);
        //$data["couleur4"] = $_POST["colpick4"];
        $url = $_POST["urlRubanLed4"]."ledAllumer";
// LG 20241129 début
//        file_post_contents($url, $data);
        $rep = file_post_contents($url, $data);
        echo "Réponse Esclave pour la demande d'animation du ruban 4 : $rep<br>" ;
// LG 20241129 fin
    }
// LG 20221125 déplacé + haut début
//     function file_post_contents_lampe($url, $data) {
//         $postdata = http_build_query($data);

//         $opts = array('http' =>
//             array(
//                 'method' => 'POST',
//                 'header' => "Content-type: application/x-www-form-urlencoded\r\n",
//                 'content' => $postdata
//             )
//         );

//         $context = stream_context_create($opts);
//         return file_get_contents($url, false, $context);
//     }
// LG 20221125 déplacé + haut fin
    
    $jsonAnim = file_get_contents("animation.json");
    $tabAnim = json_decode($jsonAnim, true);
    $jsonURL = file_get_contents("url.json");
    $tabURL = json_decode($jsonURL, true);
    $hameg = $tabURL['HAMEG'];
    $portcom = $hameg['port-com'];
    $tabEnvoi = [];
    
    if($_POST["Lampe1"] && $_POST["urlLampe1"]){
        try {
            foreach($tabAnim as $anim){
                if($anim['nom'] == $_POST['animationLampe1']){
                    $tab = array('channel' => $_POST['channelLampe1']);
                    $tab['port-com'] = $portcom;
                    $liste = $anim['liste'];
                    $tab['liste'] = $liste;
                    array_push($tabEnvoi, $tab);
                }
            }
        } catch (Exception $e) {
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }
       
    }
    if($_POST["Lampe2"] && $_POST["urlLampe2"]){
        try {
            foreach($tabAnim as $anim){
                if($anim['nom'] == $_POST['animationLampe2']){
                    $tab = array('channel' => $_POST['channelLampe2']);
                    $tab['port-com'] = $portcom;
                    $liste = $anim['liste'];
                    $tab['liste'] = $liste;
                    array_push($tabEnvoi, $tab);
                }
            }
        } catch (Exception $e) {
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }
        
    }
    if($_POST["Lampe3"] && $_POST["urlLampe3"]){
        try {
            foreach($tabAnim as $anim){
                if($anim['nom'] == $_POST['animationLampe3']){
                    $tab = array('channel' => $_POST['channelLampe3']);
                    $tab['port-com'] = $portcom;
                    $liste = $anim['liste'];
                    $tab['liste'] = $liste;
                    array_push($tabEnvoi, $tab);
                }
            }
        } catch (Exception $e) {
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }
        
    }
    if($_POST["Lampe4"] && $_POST["urlLampe4"]){
        try {
            foreach($tabAnim as $anim){
                if($anim['nom'] == $_POST['animationLampe4']){
                    $tab = array('channel' => $_POST['channelLampe4']);
                    $tab['port-com'] = $portcom;
                    $liste = $anim['liste'];
                    $tab['liste'] = $liste;
                    array_push($tabEnvoi, $tab);
                }
            }
        } catch (Exception $e) {
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }
        
    }
    
    $tabEnvoiJson = json_encode($tabEnvoi);
    $data = array('dataHameg' => $tabEnvoiJson);
    $url = $_POST["urlLampe1"]."polls/lancerAnimation";
// LG 20241129 début
//    echo $res ;
    $rep = file_post_contents_lampe($url, $data);
    echo "Réponse Esclave pour la demande d'animation Hameg : $rep<br>" ;
// LG 20241129 fin
}
 
// header('Location: /index.php'); 
exit();
