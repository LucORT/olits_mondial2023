# Olits Mondial des Métiers

> [!NOTE]
> L'OPCO Atlas a mandaté ORT-Sup pour développer une application interactive illustrant les métiers du numérique, à présenter sur son stand lors du Mondial des Métiers, avec des démonstrations prévues en décembre 2024. Réalisée par OLITS, cette application pilote une structure lumineuse en forme de pyramide via un réseau de Raspberry Pi. Une Raspberry "maître" héberge une interface web permettant de configurer des animations lumineuses, qui sont ensuite exécutées par des Raspberry "esclaves" connectées aux LEDs. La caméra de la Raspberry "maître" offre une vue en temps réel de l'animation, accessible depuis l'application.

## Sommaire

- [Olits Mondial des Métiers](#olits-mondial-des-métiers)
	- [Sommaire](#sommaire)
	- [Prérequis](#prérequis)
	- [Installation](#installation)
	- [Configuration](#configuration)
		- [Web](#web)
		- [Raspberry](#raspberry)
	- [Structure du projet](#structure-du-projet)
	- [Utilisation](#utilisation)
	- [Tests](#tests)
		- [Raspberry Pi exclaves](#raspberry-pi-exclaves)
			- [Test des leds](#test-des-leds)
			- [Stopper une animation](#stopper-une-animation)
			- [Éteindre un Raspberry](#éteindre-un-raspberry)
		- [Lancement et test du pilote de la Hameg](#lancement-et-test-du-pilote-de-la-hameg)
			- [Pour démarrer le serveur Django (PC portable)](#pour-démarrer-le-serveur-django-pc-portable)
			- [Trouver le port COM utilisé par le Hameg](#trouver-le-port-com-utilisé-par-le-hameg)
			- [Tester que la Hameg fonctionne](#tester-que-la-hameg-fonctionne)
	- [Maintenance](#maintenance)

## Prérequis

- **Python 3** : Pour exécuter les scripts Python sur les Raspberry Pi.
- **Serveur web** : Serveur pour héberger l'application web de configuration.
- **PHP v?** : V? PHP pour certaines fonctionnalités.
- **Django** : Framework Python utilisé dans le code des Raspberry "esclaves".

## Installation

1. Clonez le dépôt du projet.

```bash
git clone https://gitlab.com/LucORT/olits_mondial2023.git
```

1. Installez les dépendances Python, Django et autres.
2. Suivez les étapes de configuration pour le serveur web et les Raspberry.

## Configuration

### Web

1. Créez un serveur local pour héberger l'application (`vhost`).
2. Copiez les fichiers de configuration `.modèle` et renommez-les en supprimant l'extension `.modèle`.

### Raspberry

## Structure du projet

```plaintext
├── cameraUSB/               # Gestion de la caméra USB pour les captures d'images
├── EmulateurHameg/          # Emulateur
├── Hameg/                   #
├── Pull/                    #
├── raspberryEsclave/        # Code des Raspberry "esclaves" utilisant Django pour contrôler les LEDs
├── raspberryMaster/         # Code de la Raspberry "maître" avec l'application web de configuration
├── testp2/                  # Fichiers de test supplémentaires (détails à préciser)
├── .gitignore               # Fichier pour exclure certains fichiers du contrôle de version
├── doc_pyramide_2.docx      # Documentation technique du projet
├── Manuel.docx              # Manuel utilisateur de l'application
└── README.md                # Documentation principale du projet
```

## Utilisation

## Tests

### Raspberry Pi exclaves

Les urls vont permettre de tester des Raspberrys indépendant de l’IHM.
Tout d’abord il est important de préciser l’adresse IP de chaque appareil sur le Raspberry sur le serveur :

```bash
192.168.50.10
… . … . … . 11
… . … . … . 12
… . … . … . 13
```

#### Test des leds

Pour tester les leds vous pouvez intégrer dans url ‘testjoseph5’ qui vous permettra de vérifier le bon fonctionnement des leds.
Exemple: <http://192.168.40.10/testjoseph5>

- Ce test va exécuter l’animation noël.py, les leds vont donc s’allumer dans plusieurs couleurs.

#### Stopper une animation

Afin de stopper une animation sur une Raspberry, vous pouvez intégrer l’adresse IP de la Raspberry concernée et ‘stop’. Ce qui correspondrait à: <http://192.168.40.10/stop>

#### Éteindre un Raspberry

Pour éteindre un Raspberry, il vous faudra comme dans les précédents exemple utilisé l’adresse IP de la Raspberry concernée et du mot clé ‘Eteindre’.
Exemple : <http://192.168.40.10/Eteindre>
⛔ Attention, la Raspberry ne peut pas être rallumer avec une URL. Du faite qu’en étant éteinte, il y a aucune possibilité qu’elle puisse recevoir des informations à travers une url.

### Lancement et test du pilote de la Hameg

Le pilote de la Hameg est le PC portable.
Login : « .\infosup », mdp : « infosup »

#### Pour démarrer le serveur Django (PC portable)

- brancher le PC sur le switch "Mondial 2022", en compagnie des raspberry
- S'assurer que le PC a l'adresse : 192.168.50.14
- dans le terminal, se placer sur le répertoire "C:\Users\infosup\Desktop\git-test\olits_mondial2023\Hameg"
- taper la commande > `python manage.py runserver 192.168.50.14:8000` ou `python C:\Users\infosup\Desktop\git-test\olits_mondial2023\Hameg manage.py runserver 192.168.50.14:8000`. Le serveur doit se lancer, et afficher le message dont la dernière ligne est "Quit the server with CTRL-BREAK."

#### Trouver le port COM utilisé par le Hameg

- brancher et allumer le Hameg
- lancer le "gestionnaire de périphériques" windows de cette machine
- dans "ports (COM et LPT), trouver Hameg et libe le X de "(COMX)"
- mettre cette valeur dans tous les JSON de views.py à l'entrée "port-com" (application Django, sur cette machine)
- mettre cette valeur dans le fichier de configuration de l'application web de la raspberry maitre (\var\www\IHM\url.json)

#### Tester que la Hameg fonctionne

lancer Chrome à l'URL :

- <http://192.168.50.14:8000/polls>
- <http://192.168.50.14:8000/polls/lancerAnimation>
- <http://192.168.50.14:8000/polls/arreterAnimation>

## Maintenance

- Pour toute maintenance corrective et évolutive, contactez OLITS.
- Assurez-vous que les versions de Python, Django, et autres dépendances sont à jour selon les recommandations du projet.
