master_adress="3"
slave_addresses=("10" "11" "12" "13")

for address in "${slave_addresses[@]}"
do
	if [ "$address" != "$master_address" ]; then
		ssh pi@"192.168.50.$address" "shutdown -h  now"
		if [ $? -eq 0 ]; then
			echo "Extinction de la Raspberry Pi à l'adresse $address réussie."
		else
			echo "Echec de l'extinction pour la Raspberry Pi à l'adresse $address."
		fi
	fi
done
